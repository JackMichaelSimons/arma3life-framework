//Steve

params[
	"_healData"
	];


if (player getVariable "revived")then{	
	_damage = damage _injured;
	if !(_healer getVariable "MedicOnDuty")then{
		waitUntil {damage _injured != _damage};
		if (damage _injured != _damage) then{
			_injured setDamage 0.5;
		};
	}else{
		waitUntil {damage _injured != _damage};
		if (damage _injured < _damage ) then{
			_injured setDamage 0;
			player setVariable ["revived",false,true];
		};
	};
}else{
	_damage = damage _injured;
	waitUntil {damage _injured != _damage};
	if(damage _injured != _damage) then {
		_injured setDamage 0.65;
	};

};