/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_eventHandlers.sqf
Author: Heisen http://heisen.pw
Description: EventHandlers
Parameter(s): N/A
************************************************************/


player addEventHandler ["Fired",{ [_this select 0] call A3L_fnc_onFired; }];
player addEventHandler ["HandleDamage",{ [_this] call A3L_fnc_handleDamage; }];
player addEventHandler ["HandleHeal",{[_this] call A3L_fnc_handleHeal; }];
player addEventHandler ["onPlayerDisconnected",{[_this] call A3L_fnc_handleDisconnect; }];
player addEventHandler ["InventoryOpened",{
        if (_this select 1 isKindOf "Man") then {closeDialog 602; true}
}];

call A3L_fnc_nametags;