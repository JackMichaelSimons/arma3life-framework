//Steve

params[
	"_curOb",
	"_wheel"
];

_returnResult = false;

if(_wheel == "FL")exitWith{
//Front Left
	_hitDmg = _curOb getHit "wheel_1_1_steering";
	if(_hitDmg >= 0.25)then{
	_returnResult = true;
	_returnResult;
	};
};
if(_wheel == "FR")exitWith{
//Front Right
	_hitDmg = _curOb getHit "wheel_2_1_steering";
	if(_hitDmg >= 0.25)then{
	_returnResult = true;
	_returnResult;
	};

};
if(_wheel == "BL")exitWith{
//Back Left
	_hitDmg = _curOb getHit "wheel_1_2_steering";
	if(_hitDmg >= 0.25)then{
	_returnResult = true;
	_returnResult;
	};
};
if(_wheel == "BR")exitWith{
//Back Right
	_hitDmg = _curOb getHit "wheel_2_2_steering";
	if(_hitDmg >= 0.25)then{
	_returnResult = true;
	_returnResult;
	};
};

_returnResult;