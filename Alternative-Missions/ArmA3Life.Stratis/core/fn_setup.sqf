//["System: Waiting for Server to be Ready!",10,"blue"] call A3L_fnc_msg;
waitUntil {!isNil{A3L_Server_isReady}};
//["System: Server Ready!",10,"blue"] call A3L_fnc_msg;

//[format ["System: Checking for existing data.. (%1)",getPlayerUID player],10,"blue"] call A3L_fnc_msg;
[player] remoteExec ["A3Lsys_fnc_existPlayer",2];
//[format ["System: Check for (%1) completed!",getPlayerUID player],10,"blue"] call A3L_fnc_msg;

call A3L_fnc_variables;
call A3L_fnc_setupItems;
call A3L_fnc_eventHandlers;
[] spawn A3L_fnc_survival;
[] spawn A3L_fnc_createMarkers;

player setVariable ["trueDead", 0, true];

player setVariable ["CommunicationID",clientOwner,true];
[("Welcome to"), ("ArmA 3 Life")] spawn BIS_fnc_infoText;

waitUntil { !(isNull (findDisplay 46)) };
A3L_KeyHandler = (findDisplay 46) displayAddEventHandler ["KeyDown", "[_this] call A3L_fnc_setupKeyHandler"];

player enableSimulation false;
call A3L_fnc_spawnMenu;

42 cutRsc ["a3lhud", "PLAIN"];
[] call compile preprocessFile "dialogs\functions\a3l_fnc_initHud.sqf";
[] call compile preprocessFile "dialogs\functions\a3l_fnc_msg.sqf";