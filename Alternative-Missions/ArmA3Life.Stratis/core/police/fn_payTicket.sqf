/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_payTicket.sqf
Author: Heisen http://heisen.pw
Description: Pay Ticket
Parameter(s): N/A
************************************************************/


if (A3L_Bank <= A3L_TicketRecentValue) exitWith {
	[localize"STR_Notification_NoFunds",10,"red"] call A3L_fnc_msg;
	A3L_TicketRecentSender = nil;
	A3L_TicketRecentValue = nil;
};

[A3L_TicketRecentValue,"TAKE"] call A3L_fnc_handleBank;

[localize"STR_Notification_PaidFunds",10,"green"] call A3L_fnc_msg;

[localize"STR_Notification_PaidTicket",10,"green"] remoteExec ["A3L_fnc_msg",A3L_TicketRecentSender];
A3L_TicketRecentSender = nil;
A3L_TicketRecentValue = nil;