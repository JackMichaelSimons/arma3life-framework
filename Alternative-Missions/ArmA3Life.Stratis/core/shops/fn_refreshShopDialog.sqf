// Steve Adtur, Heisen

disableSerialization;

lbClear 1500;
lbClear 1501;

private ["_countNum","_curOb"];
_countNum = -1;

_itemArray = "true" configClasses (missionConfigFile >> "Server_Items");

_selectedIndex = lbCurSel 2100;
_selectName = lbData[2100,_selectedIndex];
_curOb = cursorObject getVariable 'shopType';
// Type of var options = BlackMarket, General, Market, FarmMarket, ResTrader, Drug, Gun,

systemChat format["Chosen Field = %1",_selectName];

_shopType = getArray(missionConfigFile >> "Server_Items" >> (format["Server_Items_%1",_curOb]));
{
	_varName = configName _x;
	_varAmount = call compile _varName;

	if(_varName IN _shopType) then {
		_shopSell = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "sellValue");
		if( _shopSell > 0)then{
			if(_varAmount >= 1)then{
				_countNum = _countNum + 1;
				_varDisplayName = getText (_x >> "displayName");
				lbAdd [1500,format["%1 x %2",_varAmount,localize(_varDisplayName)]];
				lbSetData[1500,_countNum,_varName];
			};
		};
		
	};
} forEach _itemArray;

_countNum = -1;
if (_curOb == "FBIShop") then{
	if((_selectName == "Virtual Item") or (_selectName == "All") or (_selectName == ""))exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;

			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (_x >> "displayName");
					lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
					lbSetData[1501,_countNum,_varName];
				};
				
			};
			
		}forEach _itemArray;
	};

	
	if(_selectName == "Uniforms")exitWith{
		{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "clothingType");
		if(_varType == "Uniform")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (configFile / "CfgWeapons" / _x / "displayName");
					lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
					lbSetData[1501,_countNum,_varName];
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "Vests")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "clothingType");
		if(_varType == "Vest")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (configFile / "CfgWeapons" / _x / "displayName");
					lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
					lbSetData[1501,_countNum,_varName];
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "Backpacks")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "clothingType");
		if(_varType == "Backpack")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (configFile / "CfgWeapons" / _x / "displayName");
					lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
					lbSetData[1501,_countNum,_varName];
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "Items")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "clothingType");
		if(_varType == "Item")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (configFile / "CfgWeapons" / _x / "displayName");
					lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
					lbSetData[1501,_countNum,_varName];
				};
				
			};
			
		};
		}forEach _itemArray;
	};
	if(_selectName == "Weapons")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if((_varType == "Rifle") or (_varType == "Pistol") or (_varType == "Mag"))then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{		
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
						_varDisplayName =getText (configFile / "CfgMagazines" / _x / "displayName");
						lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName =getText (configFile / "CfgWeapons" / _x / "displayName");
						lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
						lbSetData[1501,_countNum,_varName];
					};
				};
				
			};
		};
		}forEach _itemArray;
	};
}else{
	if(_curOb == "CopShop")then{
	if((_selectName == "Virtual Item") or (_selectName == "All") or (_selectName == ""))exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "copRank");
		if(_varType == "VirtualItem")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					if((_varType == "Rifle") or (_varType == "Pistol") or (_varType == "Mag"))then {
						if(_varType == "Mag")then{
						_varDisplayName =getText (configFile / "CfgMagazines" / _x / "displayName");
						lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
						_varDisplayName =getText (configFile / "CfgWeapons" / _x / "displayName");
						lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						};
					}else{
						_countNum = _countNum +1;
						_varDisplayName = getText (_x >> "displayName");
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
					};
				};
				
			};
			};
		}forEach _itemArray;
	};

	if(_selectName == "Cadet")exitWith{
		{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varCop = getText(missionConfigFile >> "Server_Items" >> _varName >> "copRank");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if(_varCop == "Cadet")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
						_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName = getText (_x >> "displayName");
						if(_varDisplayName == "")then{
							_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
							lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						}else{
							lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						};
					};
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "Deputy")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varCop = getText(missionConfigFile >> "Server_Items" >> _varName >> "copRank");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if(_varCop == "Deputy")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
						_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName = getText (_x >> "displayName");
						if(_varDisplayName == "")then{
							_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
							lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						}else{
							lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						};
					};
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "Corporal")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varCop = getText(missionConfigFile >> "Server_Items" >> _varName >> "copRank");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if(_varCop == "Corporal")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
						_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName = getText (_x >> "displayName");
						if(_varDisplayName == "")then{
							_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
							lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						}else{
							lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						};
					};
				};
				
			};
		};
		
		}forEach _itemArray;
	};
	if(_selectName == "Sergeant")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varCop = getText(missionConfigFile >> "Server_Items" >> _varName >> "copRank");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if(_varCop == "Sergeant")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
						_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName = getText (_x >> "displayName");
						if(_varDisplayName == "")then{
						_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
							lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						}else{
							lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						};
					};
				};
				
			};
			
		};
		}forEach _itemArray;
	};
	if(_selectName == "Command")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varCop = getText(missionConfigFile >> "Server_Items" >> _varName >> "copRank");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if(_varCop == "Command")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
						_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName = getText (_x >> "displayName");
						if(_varDisplayName == "")then{
						_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
							lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						}else{
							lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						};
					};
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "High Command")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varCop = getText(missionConfigFile >> "Server_Items" >> _varName >> "copRank");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if(_varCop == "High Command")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
						_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName = getText (_x >> "displayName");
						if(_varDisplayName == "")then{
							_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
							lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						}else{
							lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						};
					};
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "SERT")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varCop = getText(missionConfigFile >> "Server_Items" >> _varName >> "copRank");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if(_varCop == "SERT")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
						_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName = getText (_x >> "displayName");
						if(_varDisplayName == "")then{
						_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
							lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						}else{
							lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						};					
					};
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "SERT Command")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varCop = getText(missionConfigFile >> "Server_Items" >> _varName >> "copRank");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if(_varCop == "SERT Command")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
						_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName = getText (_x >> "displayName");
						if(_varDisplayName == "")then{
							_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
							lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						}else{
							lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						};
					};
				};
				
			};
		};
		}forEach _itemArray;
	};
	}else{
	
	if((_selectName == "Virtual Item") or (_selectName == "All") or (_selectName == ""))exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (_x >> "displayName");
					if(_varDisplayName == "")then{
						if(_varType == "Mag")then{
							_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
						}else{
						
							_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
						};
						lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
						lbSetData[1501,_countNum,_varName];
					}else{
						lbAdd [1501, format["%1 = %2",localize(_varDisplayName), _shopBuy]];
						lbSetData[1501,_countNum,_varName];
					};
				};
				
			};
			
		}forEach _itemArray;
	};

	if(_selectName == "Uniforms")exitWith{
		{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "clothingType");
		if(_varType == "Uniform")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
					lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
					lbSetData[1501,_countNum,_varName];
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "Vests")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "clothingType");
		if(_varType == "Vest")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
					lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
					lbSetData[1501,_countNum,_varName];
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "Backpacks")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "clothingType");
		if(_varType == "Backpack")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
					lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
					lbSetData[1501,_countNum,_varName];
				};
				
			};
		};
		}forEach _itemArray;
	};
	if(_selectName == "Items")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "clothingType");
		if(_varType == "Item")then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
					lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
					lbSetData[1501,_countNum,_varName];
				};
				
			};
			
		};
		}forEach _itemArray;
	};
	if(_selectName == "Weapons")exitWith{
	{
		
		_varName = configName _x;
		_varAmount = call compile _varName;
		_phsyClass = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalClass");
		_varType = getText(missionConfigFile >> "Server_Items" >> _varName >> "physicalType");
		if((_varType == "Rifle") or (_varType == "Pistol") or (_varType == "Mag"))then{
			if(_varName IN  _shopType) then {
				_shopBuy = getNumber(missionConfigFile >> "Server_Items" >> _varName >> "buyValue");
				if(_shopBuy > 0) then {
					_countNum = _countNum +1;
					if(_varType == "Mag")then{
						if((primaryWeapon player == "") && (handgunWeapon player == ""))then{
							_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
							lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
							lbSetData[1501,_countNum,_varName];
						}else{
							if(primaryWeapon player != "")then
							{
								_magArray = getArray(configFile >> "CfgWeapons" >> primaryWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							}else{
								_magArray = getArray(configFile >> "CfgWeapons" >> handgunWeapon player >> "magazines");
								if(_phsyClass IN _magArray)then{
									_varDisplayName = getText (configFile / "CfgMagazines" / _phsyClass / "displayName");
									lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
									lbSetData[1501,_countNum,_varName];
								};
							};
						};
					}else{
						_varDisplayName = getText (configFile / "CfgWeapons" / _phsyClass / "displayName");
						lbAdd [1501, format["%1 = %2",_varDisplayName, _shopBuy]];
						lbSetData[1501,_countNum,_varName];
					};
				};
				
			};
		};
		}forEach _itemArray;
	};
	};
};