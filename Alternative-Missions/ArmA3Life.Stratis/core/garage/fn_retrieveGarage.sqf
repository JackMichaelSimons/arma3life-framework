/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_retrieveGarage.sqf
Author: Heisen http://heisen.pw
Description: Retrieved List of Stored vehicles!
Parameter(s): N/A
************************************************************/


[player] remoteExec ["A3Lsys_fnc_fetchGarage",2];

diag_log format ["A3L Client Vehicle PLAYER:(%1) Retrieval Request!",getPlayerUID player];

hint format ["No Garage on PUID:(%1)",getPlayerUID player];
waitUntil {!(A3L_dataRemoteRecieved isEqualTo [])};
hint format ["Fetched Garage on PUID:(%1)",getPlayerUID player];

//--- Send Data to display via Garage Dialog
[A3L_dataRemoteRecieved] call A3L_fnc_openGarage;