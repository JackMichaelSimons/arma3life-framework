/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_openInventory.sqf
Author: Heisen http://heisen.pw
Description: Handle Inventory Menu
Parameter(s): N/A
************************************************************/


private ["_countNum"];
_countNum = -1;

if (A3L_antiSpam) exitWith {
	["You're currently performing an action!",10,"red"] call A3L_fnc_msg;
};

createDialog "RscDisplay_Inventory";

_items = "true" configClasses (missionConfigFile >> "Server_Items");

{
	_varName = configName _x;
	_varAmount = call compile _varName;
	
	systemChat format["%1 = %2",_varName,_varAmount];

	if (_varAmount >= 1) then {
		_countNum = _countNum + 1;
		_varDisplayName = getText (_x >> "displayName");
		_varWeightValue = getNumber (_x >> "weightValue");
		lbAdd [1500,format["%1x %2 - %3.kg",_varAmount,localize(_varDisplayName),(_varWeightValue * _varAmount)]];
		lbSetPicture [1500, _countNum,(getText (_x >> "displayIcon"))];
		lbSetData [1500,_countNum,_varName];
		systemChat str(_countNum);
	};
} forEach _items;