/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_mine.sqf
Author: Heisen http://heisen.pw
Description: Mine with PickAxe!
Parameter(s): N/A
************************************************************/


params [
	"_cursorObject"
];

_mineableRocks = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Mining" >> "mineableRocks");
_mineableLoot = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Mining" >> "mineableLoot");

if !((typeOf _cursorObject) IN _mineableRocks) exitWith {};

_currentDamage = cursorObject getVariable "rockHealth";
cursorObject setVariable ["rockHealth",(_currentDamage - (random(5))),true];

if (_currentDamage <= 1) exitWith {
	_position = getPos _cursorObject;
	_rockType = _mineableLoot select (_cursorObject getVariable "rockType");
	deleteVehicle _cursorObject;
	for "_i" from 0 to (round(random(3))) do {
		_newRock = createVehicle [_rockType,[(_position select 0) + random(3),(_position select 1) + random(3),(_position select 2)],[],0,"CAN_COLLIDE"];
		_newRock setVariable ["Server_Item_Data",[nil,1],true];
	};
};