/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_setupKeyHandler.sqf
Author: Heisen http://heisen.pw
Description: Setup Key Handler
Parameter(s): N/A
************************************************************/


params ["_keyData"];

_key = _keyData select 1;
_shift = _keyData select 2;
_ctrl = _keyData select 3;
_alt = _keyData select 4;

handled = false;

switch (_key) do {

	case 1: {
		if(A3L_Spawning)then{
			call A3L_fnc_spawnMenu;
		};
		if(A3L_RespawnReady)then{
			createDialog "RscDisplay_Respawn";
		};
		handled = false;
	};
	
	//--- Shift + E
	case 18: {
		if (_shift) then {
			if (A3L_inAnimation) exitWith {};
			A3L_inAnimation = true;
			[true,"PUNCH"] spawn A3L_fnc_doAction;
		};
		handled = true;
	};
	
	//--- Y
	case 44: {	
		if(!dialog)then{
			[cursorObject] call A3L_fnc_loadInteractionRadial;
			handled = true;
		};
	};
	
	//--- Unlock/Lock Vehicle
	case 22: {
	
		if (cursorObject isKindOf "Car") then {
			_vehicleData = cursorObject getVariable "vehicleData";
			if ((getPlayerUID player) IN (_vehicleData select 1)) then {
				if ((locked cursorObject) == 2) then {
					cursorObject setVehicleLock "UNLOCKED";
					hint "Vehicle Unlocked.";
				} else {
					
					cursorObject setVehicleLock "LOCKED";
					hint "Vehicle Locked.";
				};
			} else {
				hint "You don't have Keys for this Vehicle!";
			};
		};
		_veh = vehicle player;
		if(_veh != player)then{
			if (_veh isKindOf "Car") then {
				_vehicleData = _veh getVariable "vehicleData";
				if ((getPlayerUID player) IN (_vehicleData select 1)) then {
					if ((locked _veh) == 2) then {					
						_veh setVehicleLock "UNLOCKED";
						hint "Vehicle Unlocked.";
					} else {
						_veh setVehicleLock "LOCKED";
						hint "Vehicle Locked.";
					};
				} else {
					hint "You don't have Keys for this Vehicle!";
				};
			};
		};
		handled = true;
	};
	
	case 35: {
		if(_shift)then{
			if(currentWeapon player != "")then{
				player action ["SwitchWeapon", player, player, 100];
			};
		
		};
	
	handled = true;
	};

};

handled;