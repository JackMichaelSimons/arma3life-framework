/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_variables.sqf
Author: Heisen http://heisen.pw
Description: Handle Usage of an Item
Parameter(s): N/A
************************************************************/

A3L_SyncRecent = false;
A3L_dataRemoteRecieved = [];

A3L_Jailed = false;

A3L_Bank = 1000;

A3L_Thirst = 100;
A3L_Hunger = 100;

A3L_Weight = 0;
A3L_MaxCarry = getNumber (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "maxCarry");

A3L_antiSpam = false;

A3L_copOnDuty = false;
A3L_medicOnDuty = false;

A3L_Spawning = true;
A3L_VehicleInteract = "";

A3L_RespawnReady = false;

A3L_SandMiner = 0;
A3L_inAnimation = false;

player setVariable ["_onDrugs",false,true];
player setVariable ["_showName",true,true];

player setVariable ["A3L_Tazed",false,true];
player setVariable ["A3L_cuffed",0,true]; //0: not restrain | 1: restrain | 2: floor | 3: walk
player setVariable ["A3L_drag",false,true];

player setVariable ["A3L_NameSetting",false,true];
player setVariable ["A3L_CopOnDuty",false,true];