class RscDisplay_BankWindow
{
	idd = 19875;
	movingEnabled = false;
	
	class controls
	{

		class Sikoras_rscPic: A3LRscPicture
		{
			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.341345 * safezoneW + safezoneX;
			y = 0.340133 * safezoneH + safezoneY;
			w = 0.304089 * safezoneW;
			h = 0.178675 * safezoneH;
			colorText[] = {1,1,1,0.5};
		};
		class Sikoras_text: A3LRscStructuredText
		{
			idc = 1100;
			text = "0";
			x = 0.36338 * safezoneW + safezoneX;
			y = 0.358941 * safezoneH + safezoneY;
			w = 0.13662 * safezoneW;
			h = 0.0564236 * safezoneH;
		};
		class Sikoras_edit: A3LRscEdit
		{
			idc = 1400;
			text = "0";
			x = 0.508814 * safezoneW + safezoneX;
			y = 0.358941 * safezoneH + safezoneY;
			w = 0.123399 * safezoneW;
			h = 0.0564236 * safezoneH;
		};
		class Sikoras_button1: A3LRscShortcutButtoneco
		{
			idc = 1700;
			text = "Withdraw Cash"; //--- ToDo: Localize;
			x = 0.36338 * safezoneW + safezoneX;
			y = 0.443576 * safezoneH + safezoneY;
			w = 0.132213 * safezoneW;
			h = 0.0564236 * safezoneH;
			onButtonClick = "call A3L_fnc_Withdraw";
		};
		class Sikoras_button2: A3LRscShortcutButtoneco
		{
			idc = 1701;
			text = "Deposit Cash"; //--- ToDo: Localize;
			x = 0.504407 * safezoneW + safezoneX;
			y = 0.443576 * safezoneH + safezoneY;
			w = 0.132213 * safezoneW;
			h = 0.0564236 * safezoneH;
			onButtonClick = "call A3L_fnc_Deposit";
		};


		
		
	};	
	
};