class RscDisplay_ShopWindow
{
	idd = 19874;
	movingEnabled = false;
	
	class controls
	{
		class Sikoras_rscPic: A3LRscPicture
		{
			idc = 1200;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.363379 * safezoneW + safezoneX;
			y = 0.217883 * safezoneH + safezoneY;
			w = 0.268833 * safezoneW;
			h = 0.611255 * safezoneH;
			colorText[] = {1,1,1,0.5};
		};
		class Sikoras_rscList: A3LRscListbox
		{
			idc = 1500;
			x = 0.367787 * safezoneW + safezoneX;
			y = 0.28371 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.413773 * safezoneH;
		};
		class Sikoras_rscList_1: A3LRscListbox
		{
			idc = 1501;
			x = 0.517628 * safezoneW + safezoneX;
			y = 0.31371 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.413773 * safezoneH;
		};
		
		class Sikoras_RscCombo: A3LRscCombo
		{
			idc = 2100;
			x = 0.517628 * safezoneW + safezoneX;
			y = 0.28371 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.0282118 * safezoneH;
			onLBSelChanged = "call A3L_fnc_refreshShopDialog";
		};
		class Sikoras_rscButton_1: A3LRscShortcutButtoneco
		{
			idc = 1600;
			text = "Sell Product"; //--- ToDo: Localize;
			x = 0.367787 * safezoneW + safezoneX;
			y = 0.76331 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "call A3L_fnc_sellProduct";
		};
		class Sikoras_rscButton_2: A3LRscShortcutButtoneco
		{
			idc = 1601;
			text = "Buy Product"; //--- ToDo: Localize;
			x = 0.517628 * safezoneW + safezoneX;
			y = 0.76331 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.0470196 * safezoneH;
			onButtonClick = "call A3L_fnc_buyProduct";
		};
		class Sikoras_rscText_1: A3LRscStructuredText
		{
			idc = 1100;
			text = "My Inventory"; //--- ToDo: Localize;
			x = 0.367787 * safezoneW + safezoneX;
			y = 0.227286 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.0470196 * safezoneH;
			colorText[] = {0,0,0,1};
			colorBackground[] = {1,1,1,1};
		};
		class Sikoras_rscText_2: A3LRscStructuredText
		{
			idc = 1101;
			text = "Store Stock"; //--- ToDo: Localize;
			x = 0.517628 * safezoneW + safezoneX;
			y = 0.227286 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.0470196 * safezoneH;
			colorText[] = {0,0,0,1};
			colorBackground[] = {1,1,1,1};
		};
		class RscEdit_1400: A3LRscEdit
		{
			idc = 1400;
			text = "1";
			x = 0.367787 * safezoneW + safezoneX;
			y = 0.706886 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.0470196 * safezoneH;
		};
		class RscEdit_1401: A3LRscEdit
		{
			idc = 1401;
			text = "1";
			x = 0.517628 * safezoneW + safezoneX;
			y = 0.706886 * safezoneH + safezoneY;
			w = 0.110177 * safezoneW;
			h = 0.0470196 * safezoneH;
		};

	};
		
};



