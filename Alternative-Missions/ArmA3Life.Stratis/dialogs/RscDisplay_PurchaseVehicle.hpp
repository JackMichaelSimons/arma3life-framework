class RscDisplay_PurchaseVehicle {
    idd = 1003;
    name= "RscDisplay_PurchaseVehicle";
	movingEnable=true;
	onLoad = "";
	onUnLoad = "";
    class controlsBackground {
    };

    class controls {

		class A3LRscDisplay_Main: A3LRscPicture
		{
			idc = 1200;
			moving = 1;
			text = "dialogs\data\menu_base.paa";
			x = 0.355625 * safezoneW + safezoneX;
			y = -0.16 * safezoneH + safezoneY;
			w = 0.2475 * safezoneW;
			h = 0.935 * safezoneH;
		};
		class A3LRscShortcutButton_1700: A3LRscShortcutButtoneco
		{
			idc = 1700;
			text = "Purchase Vehicle"; //--- ToDo: Localize;
			onButtonClick = "[cursorObject,[]] call A3L_fnc_buyVehicle;";
			x = 0.407187 * safezoneW + safezoneX;
			y = 0.786 * safezoneH + safezoneY;
			w = 0.195937 * safezoneW;
			h = 0.033 * safezoneH;
		};
		class A3LRscDisplay_PictureVehicle: A3LRscPicture
		{
			idc = 1201;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.269 * safezoneH + safezoneY;
			w = 0.175313 * safezoneW;
			h = 0.242 * safezoneH;
		};
		class A3LRscStructuredText_1100: A3LRscText
		{
			idc = 1100;
			text = "vehicle name"; //--- ToDo: Localize;
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.236 * safezoneH + safezoneY;
			w = 0.175313 * safezoneW;
			h = 0.022 * safezoneH;
		};
		class A3LRscStructuredText_1101: A3LRscStructuredText
		{
			idc = 1101;
			text = "vehicle info"; //--- ToDo: Localize;
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.522 * safezoneH + safezoneY;
			w = 0.175313 * safezoneW;
			h = 0.187 * safezoneH;
		};
		class A3LRscSlider_1900: A3LRscSlider
		{
			idc = 1900;
			x = 0.4175 * safezoneW + safezoneX;
			y = 0.731 * safezoneH + safezoneY;
			w = 0.144375 * safezoneW;
			h = 0.033 * safezoneH;
		    onSliderPosChanged = "hint format[""%1"",_this];";
		};
		class A3LRscPicture_1202: A3LRscPicture
		{
			idc = 1202;
			text = "#(argb,8,8,3)color(1,1,1,1)";
			x = 0.567031 * safezoneW + safezoneX;
			y = 0.72 * safezoneH + safezoneY;
			w = 0.0257812 * safezoneW;
			h = 0.044 * safezoneH;
		};
	
    };
};

