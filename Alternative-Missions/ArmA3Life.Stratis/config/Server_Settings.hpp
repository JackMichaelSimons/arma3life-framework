class Server_Settings
{
	serverName = "";
	serverVersion = "0.6.0";

	class Save_Settings
	{
		VehicleInventorySaveLoad = 0;
	};
	
	class Player_Settings
	{
		maxCarry = 50;

		class Player_Survival
		{
			class Hunger
			{
				decrease = 1.5;
				decreaseSleep = 54; // 0 Minute, 54 seconds
			};
			class Thirst
			{
				decrease = 1.5;
				decreaseSleep = 54; // 0 Minute, 54 seconds
			};
		};

		class Player_Refining
		{
			refineTimer = 100; 
		};
		
		class Player_Medical
		{
			reviveTimer = 30;
			respawnTimer = 600;
		};
		
		class Player_Weapons
		{
			meleeWeapons[] = {"A3L_Pickaxe2017","A3L_Hatchet2017","A3L_Spade2017","MeleeHatchet","MeleeScythe"};
			nonLethalRounds[] = {"26_taser","BP_762x51_Ball_Rubber"};
			tranqRounds[] = {"BP_762x51_Ball_Rubber"};
		};

		class Player_Mining
		{
			mineableRocks[] = {"A3L_Bits_Rock_L_Coal","A3L_Bits_Rock_L_Iron"};
			mineableLoot[] = {"A3L_Bits_Rock_S_Coal","A3L_Bits_Rock_S_Iron"};
			sandSurfaceTypes[] = {"#GdtStratisBeach"}; // need to get surface type names in dev private testing via surfaceType (getPos player)
		};
		
		class Player_WoodCutting
		{
			woodCutting[] = {"Paper_Mulberry"};
			woodCuttingLoot[] = {"A3L_WoodLog"};
		};
		
		class Player_Farming
		{
			farmingPlants[] = {"A3L_Cannabis","Oleander2","A3L_Wheat","A3L_Corn","A3L_Beans","A3L_Cotton","A3L_Pumpkin","A3L_Sunflower"};
			farmingSurfaces[] = {"#GdtDirt","#GdtGrassGreen","#GdtGrassShort","#GdtGrassTall","#GdtStratisGreenGrass"}; // need to get surface type names in dev private testing via surfaceType (getPos player)
			farmingGround[] = {1.5,1.5,2,1.5,1.5,1.5,1.5,1.5};
			farmingTooClose = 3;
			farmingTimer[] = {600,600,300,300,300,300,300,300};
		};
		
		class Player_Antagonistic
		{
			robberyTimer = 120;
			robberyBase = 4000;
		};
		
	};
	class Player_Garages
	{
		garages[] = {"A3L_Garage_1","A3L_Garage_2"};
	};
	class Player_atm
	{ 
		atms[] = {"A3L_ATM_1","A3L_ATM_2"};
	};
	class Player_Spawns
	{
		spawnPoints[] = {"Union City","Falls Church"};
	};
};


#include "Server_Items.hpp"
#include "Server_Vehicles.hpp"
#include "Server_Interactions.h"
#include "Server_Crafting.hpp"
#include "Server_Medical.hpp"