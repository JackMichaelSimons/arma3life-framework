class Server_Items
{
	
	Server_Items_Thirst[] = {"Item_WaterBottle","Item_CocaCola"};
	Server_Items_Hunger[] = {"Item_SixFaggots"};
	Server_Items_Crafting[] = {"Item_A3Rail","Item_A3Reciever","Item_A4Rail","Item_A4Reciever","Item_AK47Barrel","Item_AK47Folding","Item_AK47Reciever","Item_AK74Barrel","Item_AK74Folding","Item_AK74Reciever","Item_AK74Stock","Item_ColtBarrel","Item_ColtReciever","Item_Foregrip","Item_GreenPaint","Item_HK10Rail","Item_HK14Rail","Item_HKReciever","Item_M4Rail","Item_M4Reciever","Item_TanPaint"};
	Server_Items_Unrefined[] = {"Item_IronOre","Item_CoalOre","Item_WoodLog","Item_Oil","Item_CannabisBud","Item_Psuedo","Item_UnpressedBenzo"};
	Server_Items_Refined[] = {"Item_IronIngot","Item_RefinedCoal","Item_TreatedWood","Item_RefinedOil","Item_Cannabis","Item_Amphet","Item_Benzo"};
	Server_Items_Seeds[] = {"Item_CannabisSeed"};
	Server_Items_HarvestedPlants[] = {"Item_CannabisBud"};	//Picked Plants
	Server_Items_FarmMarket[] = {}; // Seeds and Plants
	Server_Items_ResourceTrader[] = {"Item_IronIngot","Item_RefinedCoal","Item_TreatedWood","Item_RefinedOil"};
	Server_Items_Market[] = {"Item_WaterBottle","Item_CocaCola","Item_SixFaggots"};
	Server_Items_DrugDealer[] = {"Item_CannabisSeed","Item_Cannabis","Item_Benzo","Item_Amphet"};
	Server_Items_BlackMarket[] = {"Item_Journalist","Item_Bandanna_khk"};
	Server_Items_General[] = {};
	Server_Items_Gun[] = {"Item_RH_m9","Item_RH_m1911","Item_RH_ttracker","Item_RH_ttracker_g","Item_RH_kimber","Item_RH_kimber_nw","Item_RH_fn57","Item_RH_mateba","Item_bp_Crossbow","Item_bp_SW45","Item_bp_Crossbow_Mag","Item_bp_SW_Mag"};
	Sevrer_Items_Rifle[] = {"Item_bp_CZ452","Item_bp_Lupara","Item_bp_Benelli","Item_bp_Remington","Item_bp_Ruger","Item_bp_1866","Item_bp_1866Collector","Item_bp_CZ452_Mag","Item_bp_2Shot_Mag","Item_bp_8Shot_Mag","Item_bp_Ruger_Mag","Item_bp_Winchester_Mag"};
	Server_Items_Illegal[] = {"Item_CannabisSeed","Item_CannabisBud","Item_Cannabis","Item_Psuedo","Item_UnpressedBenzo","Item_Amphet","Item_Benzo"};
	Server_Items_CopShop[] = {};
	Server_Items_FBIShop[] = {};
	Server_Items_Drugs[] = {"Item_Heroin","Item_Amphet","Item_Benzo"};
	

	class Item_Cash
 	{
 	displayName = "STR_Items_Cash";
 	displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
 	displayModel = "Land_Money_F";
 	weightValue = 0;
 	};
 	class Item_SpareWheel
	{
		displayName = "STR_Items_SpareWheel";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 20;
		buyValue = 250;
		sellValue = 0;
	};
 	class Item_Sand
	{
		displayName = "STR_Items_Sand";
		displayIcon = "\Life_Client\Icons\Default\ico_sand.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 20;
		buyValue = 250;
		sellValue = 0;
	};
	class Item_WaterBottle
	{
		displayName = "STR_Items_WaterBottle";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_BottlePlastic_V2_F";
		buyValue = 1.31;
		sellValue = 1.00;
		weightValue = 0.5;
		thirst = 10;
	};
	class Item_CocaCola
	{
		displayName = "STR_Items_CocaCola";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Pillow_grey_F";
		buyValue = 1.5;
		sellValue = 1.1;
		weightValue = 0.5;
		thirst = 10;
	};
	class Item_SixFaggots
	{
		displayName = "STR_Items_SixFaggots";
		displayIcon = "\Life_Client\Icons\Default\ico_waterBottle.paa";
		displayModel = "Land_Pillow_grey_F";
		buyValue = 1.5;
		sellValue = 1.1;
		weightValue = 0.5;
		hunger = 10;
	};
	class Item_IronOre
	{
		displayName = "STR_Items_IronOre";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3L_Bits_Rock_S_Iron";
		weightValue = 0.5;
		refineryStats[] = {3,1};
		refined = "Item_IronIngot";
		RefineryType = "Smelt";
	};
	class Item_CoalOre
	{
		displayName = "STR_Items_CoalOre";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3L_Bits_Rock_S_Coal";
		weightValue = 0.5;
		refineryStats[] = {3,1};
		refined = "Item_RefinedCoal";
		RefineryType = "Smelt";
	};
	class Item_IronIngot
	{
		displayName = "STR_Items_IronIngot";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; 
		weightValue = 0.5;
		sellValue = 50;
		buyValue = 0;
	};
	class Item_RefinedCoal
	{
		displayName = "STR_Items_RefinedCoal";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3L_RefinedCoal"; // NEED MODEL
		weightValue = 0.5;
		sellValue = 50;
		buyValue = 0;
	};
	class Item_TreatedWood
	{
		displayName = "STR_Items_TreatedWood";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3L_TreatedWood"; // NEED MODEL
		weightValue = 0.5;
		sellValue = 50;
		buyValue = 0;
	};
	class Item_WoodLog
	{
		displayName = "STR_Items_WoodLog";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3L_WoodLog";
		refineryStats[] = {2,1};
		refined = "Item_TreatedWood";
		RefineryType = "LumberFac";
		weightValue = 0.5;
	};
	class Item_Oil
	{
		displayName = "STR_Items_Oil";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3L_RefinedOil"; // NEED MODEL
		refineryStats[] = {5,1};
		refined = "Item_RefinedOil";
		RefineryType = "ChemPlant";
		weightValue = 0.5;
		
	};
	class Item_RefinedOil
	{
		displayName = "STR_Items_RefinedOil";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3L_RefinedOil"; // NEED MODEL
		weightValue = 0.5;
		sellValue = 50;
		buyValue = 0;
	};
	class Item_CannabisSeed
	{
		displayName = "STR_Items_CannabisSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3L_SeedBox_Cannabis"; 
		weightValue = 0.5;
	};
	class Item_CannabisBud
	{
		displayName = "STR_Items_CannabisBud";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		refineryStats[] = {3,1};
		refined = "Item_Cannabis";
		RefineryType = "DrugProc";
		weightValue = 0.5;
		buyValue = 50;
		sellValue = 0;
	};
	class Item_Cannabis
	{
		//Sellable Prodcut
		displayName = "STR_Items_Cannabis";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 0;
		sellValue = 100;
	};
	class Item_OpiumSeed
	{
		displayName = "STR_Items_OpiumSeed";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "A3L_SeedBox_Cannabis"; 
		weightValue = 0.5;
	};
	class Item_OpiumFlower
	{
		displayName = "STR_Items_OpiumFlower";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		refineryStats[] = {3,1};
		refined = "Item_Cannabis";
		RefineryType = "DrugProc";
		weightValue = 0.5;
		buyValue = 50;
		sellValue = 0;
	};
	class Item_Heroin
	{
		//Sellable Prodcut
		displayName = "STR_Items_Heroin";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 0;
		sellValue = 100;
	};
	class Item_Psuedo
	{
		displayName = "STR_Items_Psuedo";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		refineryStats[] = {3,1};
		refined = "Item_Amphet";
		RefineryType = "DrugProc";
	};
	class Item_Amphet
	{
		displayName = "STR_Items_Amphet";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 0;
		sellValue = 100;
	};
	class Item_UnpressedBenzo
	{
		displayName = "STR_Items_UnBenzo";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		refineryStats[] = {3,1};
		refined = "Item_Benzo";
		RefineryType = "DrugProc";
	};
	class Item_Benzo
	{
		displayName = "STR_Items_Benzo";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 0;
		sellValue = 100;
	};
	class Item_GHB
	{
		displayName = "STR_Items_UnGHB";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		refineryStats[] = {3,1};
		refined = "Item_GHBWater";
		RefineryType = "DrugProc";
	};
	class Item_GHBWater
	{
		displayName = "STR_Items_GHB";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F";
		weightValue = 0.5;
		buyValue = 0;
		sellValue = 100;
	};
	class Item_Handcuff_Normal
	{
		displayName = "STR_Items_HandCuff_Normal";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 50;
		sellValue = 0;
	};
	class Item_Handcuff_Kinky
	{
		displayName = "STR_Items_HandCuff_Kinky";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 50;
		sellValue = 0;
	};
	class Item_Zipties
	{
		displayName = "STR_Items_Zipties";
		displayIcon = "\Life_Client\Icons\Default\ico_ironore.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		buyValue = 50;
		sellValue = 0;
	};
	class Item_Journalist
	{
		displayName = "";
		buyValue = 100;
		physicalClass = "U_C_Journalist";
		physicalType = "Cloth";
		clothingType = "Uniform";
	};
	class Item_Bandanna_khk
	{
		displayName = "";
		buyValue = 50;
		physicalClass = "H_Bandanna_khk";
		physicalType = "Cloth";
		clothingType = "Item";
	};
	// Pistols - RH Weapons
	class Item_RH_Glock18
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_g18";
		physicalType = "Pistol";
	};
	class Item_RH_Glock22
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_g22";
		physicalType = "Pistol";
	};
	class Item_RH_p220
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_p220";
		physicalType = "Pistol";
	};
    class Item_RH_m9
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_m9";
		physicalType = "Pistol";
	};
	class Item_RH_m1911
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_m1911";
		physicalType = "Pistol";
	};
	class Item_RH_ttracker
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_ttracker";
		physicalType = "Pistol";
	};
	class Item_RH_ttracker_g
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_ttracker_g";
		physicalType = "Pistol";
	};
	class Item_RH_gsh18
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_gsh18";
		physicalType = "Pistol";
	};
	class Item_RH_kimber
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_kimber";
		physicalType = "Pistol";
	};
	class Item_RH_kimber_nw
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_kimber_nw";
		physicalType = "Pistol";
	};
	class Item_RH_bull
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_bullb";
		physicalType = "Pistol";
	};
	class Item_RH_bull_black
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_bullb";
		physicalType = "Pistol";
	};
	class Item_RH_fn57
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_fn57";
		physicalType = "Pistol";
	};
	class Item_RH_fnp45
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_fnp45";
		physicalType = "Pistol";
	};
	class Item_RH_tec9
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_tec9";
		physicalType = "Pistol";
	};
	class Item_RH_vz61
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_vz61";
		physicalType = "Pistol";
	};
	class Item_RH_deagle
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_deagle";
		physicalType = "Pistol";
	};
	class Item_RH_deagle_gold
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_deagleg";
		physicalType = "Pistol";
	};
	class Item_RH_deagle_silver
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_deagles";
		physicalType = "Pistol";
	};
	class Item_RH_deagle_black
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_deaglem";
		physicalType = "Pistol";
	};
	class Item_RH_usp
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_usp";
		physicalType = "Pistol";
	};
	class Item_RH_mateba
	{
		displayName = "";
		buyValue = 5000;
		physicalClass = "RH_mateba";
		physicalType = "Pistol";
	};
    // Rifles - RH Weapons
	class Item_RH_M16A3
	{
		displayName = "";
		buyValue = 1000;
		physicalClass = "RH_M16A3";
		physicalType = "Rifle";
	};
	class Item_RH_M16A4
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M16A4_m";
		physicalType = "Rifle";
	};
	class Item_RH_M16A4_des
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M16A4_des";
		physicalType = "Rifle";
	};
	class Item_RH_M16A4_wdl
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M16A4_wdl";
		physicalType = "Rifle";
	};
	class Item_RH_M4A1_ris
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M4A1_ris";
		physicalType = "Rifle";
	};
	class Item_RH_M4A1_ris_des
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M4A1_ris_des";
		physicalType = "Rifle";
	};
	class Item_RH_M4A1_ris_wdl
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M4A1_ris_wdl";
		physicalType = "Rifle";
	};
	class Item_RH_HK416
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_Hk416";
		physicalType = "Rifle";
	};
	class Item_RH_HK416s
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_Hk416s";
		physicalType = "Rifle";
	};
	class Item_RH_M4_moe
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M4_moe";
		physicalType = "Rifle";
	};
	class Item_RH_M4_moe_green
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M4_moe_g";
		physicalType = "Rifle";
	};
	class Item_RH_M4_moe_black
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M4_moe_b";
		physicalType = "Rifle";
	};
	class Item_RH_M4_moe_Gold
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M4_moe_Gold";
		physicalType = "Rifle";
	};
	class Item_RH_M4_moe_Chrome
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_M4_moe_Chrome";
		physicalType = "Rifle";
	};
	class Item_RH_ar10
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_ar10";
		physicalType = "Rifle";
	};
	class Item_RH_Mk12mod1
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "RH_Mk12mod1";
		physicalType = "Rifle";
	};
	 // Rifles - HLC Weapons
	class Item_hlc_ak12
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_ak12";
		physicalType = "Rifle";
	};
	class Item_hlc_ak74
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_ak74";
		physicalType = "Rifle";
	};
	class Item_hlc_aks74
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_aks74";
		physicalType = "Rifle";
	};
	class Item_hlc_akm
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_akm";
		physicalType = "Rifle";
	};
	class Item_hlc_mp510
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_smg_mp510";
		physicalType = "Rifle";
	};
	class Item_hlc_mp5a4
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_smg_mp5a4";
		physicalType = "Rifle";
	};
	class Item_hlc_bcmjack
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_bcmjack";
		physicalType = "Rifle";
	};
	class Item_hlc_bcmblackjack
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_bcmblackjack";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_Gold
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_Gold";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_Chrome
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_Chrome";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_Supreme
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_Supreme";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_Gucci
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_Gucci";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_LV
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_LV";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_aqua
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_aqua";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_blue
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_blue";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_MTP
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_MTP";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_navy
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_navy";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_pink
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_pink";
		physicalType = "Rifle";
	};
	class Item_hlc_CQBR_white
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "hlc_rifle_CQBR_white";
		physicalType = "Rifle";
	};
// BP Weapons
	class Item_bp_AR15
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_M16OLD";
		physicalType = "Rifle";
	};
	class Item_bp_CZ452
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_CZ455s";
		physicalType = "Rifle";
	};
	class Item_bp_Enfield
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_LeeEnfield2";
		physicalType = "Rifle";
	};
	class Item_bp_Kar
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_Kar98";
		physicalType = "Rifle";
	};
	class Item_bp_Lupara
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_Lupara";
		physicalType = "Rifle";
	};
	class Item_bp_Garand
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_GarandU";
		physicalType = "Rifle";
	};
	class Item_bp_Crossbow
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_Crossbow";
		physicalType = "Rifle";
	};
	class Item_bp_Benelli
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_Benelli";
		physicalType = "Rifle";
	};
	class Item_bp_Remington
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_Rem870";
		physicalType = "Rifle";
	};
	class Item_bp_Ruger
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_Ruger";
		physicalType = "Rifle";
	};
	class Item_bp_Springfield
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_M1903";
		physicalType = "Rifle";
	};
	class Item_bp_SVT
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_SVT40";
		physicalType = "Rifle";
	};
	class Item_bp_1866
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_1866";
		physicalType = "Rifle";
	};
	class Item_bp_1866Collector
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_1866C";
		physicalType = "Rifle";
	};
	class Item_bp_M93R
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_m9Tac";
		physicalType = "Pistol";
	};
	class Item_bp_Uzi
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_Uzi";
		physicalType = "Pistol";
	};
	class Item_bp_SA61
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_SA61";
		physicalType = "Pistol";
	};
	class Item_bp_SW45
	{
		displayName = "";
		buyValue = 10000;
		physicalClass = "BP_SW45";
		physicalType = "Pistol";
	};
	class Item_bp_AKM_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_762x39_AKM";
		physicalType = "Mag";
	};
	class Item_bp_CZ452NL_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_5Rnd_762Rubber_Mag";
		physicalType = "Mag";
	};
	class Item_bp_AR15_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_556x45_Stanag";
		physicalType = "Mag";
	};
	class Item_bp_CZ452_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_5Rnd_22_Mag";
		physicalType = "Mag";
	};
	class Item_bp_Enfield_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_10Rnd_303_Mag";
		physicalType = "Mag";
	};
	class Item_bp_Kar_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_5Rnd_Mauser_Mag";
		physicalType = "Mag";
	};
	class Item_bp_2Shot_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_2Rnd_Buckshot";
		physicalType = "Mag";
	};
	class Item_bp_M1_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_8Rnd_3006_Mag";
		physicalType = "Mag";
	};
	class Item_bp_Crossbow_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_Arrow_Mag";
		physicalType = "Mag";
	};
	class Item_bp_8Shot_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_8Rnd_Buckshot";
		physicalType = "Mag";
	};
	class Item_bp_Ruger_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_25Rnd_22_Mag";
		physicalType = "Mag";
	};
	class Item_bp_Spring_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_5Rnd_3006_Mag";
		physicalType = "Mag";
	};
	class Item_bp_SVT_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_762x54_SVD";
		physicalType = "Mag";
	};
	class Item_bp_Winchester_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_7Rnd_45acp";
		physicalType = "Mag";
	};
	class Item_bp_M93R_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_33Rnd_9x19OVP";
		physicalType = "Mag";
	};
	class Item_bp_Uzi_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_30Rnd_9x21_Mag";
		physicalType = "Mag";
	};
	class Item_bp_Skorpion_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_20Rnd_765x17";
		physicalType = "Mag";
	};
	class Item_bp_SW_Mag
	{
		displayName = "";
		buyValue = 500;
		physicalClass = "BP_6Rnd_45_Mag";
		physicalType = "Mag";
	};

	class Item_ExampleMag
	{
		displayName = "STR_Items_M16A3";
		buyValue = 1000;
		physicalClass = "RH_M16A3";
		physicalType = "Mag";
	};

	class Item_A4Rail
	{
		displayName = "STR_Items_A4RailSystem";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "A3L_A4Rail";
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",20,"STR_Items_IronIngot"},{"Item_RefinedCoal",10,"STR_Items_RefinedCoal"}};
	};
	class Item_A4Reciever
	{
		displayName = "STR_Items_A4Reciever";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "A3L_A4Reciever";
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",20,"STR_Items_IronIngot"},{"Item_RefinedCoal",10,"STR_Items_RefinedCoal"}};
	};
	class Item_A3Rail
	{
		displayName = "STR_Items_A3Rail";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_A3Reciever
	{
		displayName = "STR_Items_A3Reciever";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_M4Rail
	{
		displayName = "STR_Items_M4Rail";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_M4Reciever
	{
		displayName ="STR_Items_M4Reciever";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_ColtBarrel
	{
		displayName ="STR_Items_ColtBarrel";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_ColtReciever
	{
		displayName ="STR_Items_ColtReciever";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_Foregrip
	{
		displayName ="STR_Items_Foregrip";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"}};
	};
	class Item_HKReciever
	{
		displayName ="STR_Items_HKReciever";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_HK10Rail
	{
		displayName ="STR_Items_HK10";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_HK14Rail
	{
		displayName ="STR_Items_HK14";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_GreenPaint
	{
		displayName ="STR_Items_GreenPaint";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		buyValue = 10;
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_RefinedOil",10,"STR_Items_RefinedOil"}};
	};
	class Item_TanPaint
	{
		displayName ="STR_Items_TanPaint";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		buyValue = 10;
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_RefinedOil",10,"STR_Items_RefinedOil"}};
	};
	class Item_AK74Reciever
	{
		displayName ="STR_Items_AK74Reciever";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_AK74Barrel
	{
		displayName ="STR_Items_AK74Barrel";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"},{"Item_TreatedWood",5,"STR_Items_TreatedWood"}};
	};
	class Item_AK74Stock
	{
		displayName ="STR_Items_AK74Stock";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"},{"Item_TreatedWood",5,"STR_Items_TreatedWood"}};
	};
	class Item_AK74Folding
	{
		displayName ="STR_Items_AK74Folding";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_AK47Reciever
	{
		displayName ="STR_Items_AK47Reciever";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};
	class Item_AK47Barrel
	{
		displayName ="STR_Items_AK47Barrel";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"},{"Item_TreatedWood",5,"STR_Items_TreatedWood"}};
	};
	class Item_AK47Folding
	{
		displayName ="STR_Items_AK47Folding";
		displayIcon = "\Life_Client\Icons\Default\ico_RECIEVER.paa";
		displayModel = "Land_Pillow_grey_F"; // NEED MODEL
		weightValue = 0.5;
		materialsNeeded[] = {{"Item_IronIngot",10,"STR_Items_IronIngot"},{"Item_RefinedCoal",5,"STR_Items_RefinedCoal"}};
	};

	
	 //-- END: Weapon Parts  --\\
	

};