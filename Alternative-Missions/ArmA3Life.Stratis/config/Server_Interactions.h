class cfgInteractions
{
	// DO NOT ALTER THE BELOW 
	class OpenSettingsMenu {
		title = "Settings";
		action = "['cfgInteractionsSettings'] spawn A3L_fnc_InteractionSubRadial";
		check = "(isPlayer player)";
	};
	class OpenInventory {
		title = "Inventory";
		action = "[] call A3L_fnc_openInventory";
		check = "(isPlayer player)";
	};
	class UseShop {
		title = "Interact";
		action = "call A3L_fnc_showShopDialog";
		check = "(cursorObject getVariable 'A3LShop')";
	};
	class UseCopShop {
		title = "Interact";
		action = "call A3L_fnc_showCopShopDialog";
		check = "(cursorObject getVariable 'A3LShop')";
	};
	class CopSignOn {
		title = "SO - Switch Duty";
		action = "['cop'] spawn A3L_fnc_dutySwitch";
		check = "(cursorObject getVariable 'so_signon')";
	};
	class MedicSignon {
		title = "Medic - Switch Duty";
		action = "['medic'] spawn A3L_fnc_dutySwitch";
		check = "(cursorObject getVariable 'medic_signon')";
	};
	// DO NOT ALTER THE ABOVE ^^
	class OpenGarage {
		title = "Open Garage";
		action = "[] spawn A3L_fnc_retrieveGarage";
		check = "(call A3L_fnc_garageCheck)";
	};
	class StoreGarage {
		title = "Store Vehicle";
		action = "[cursorObject] call A3L_fnc_storeVehicle";
		check = "(call A3L_fnc_garageCheck)";
	};
	class BuyVehicle {
		title = "Purchase Vehicle";
		action = "[cursorObject] call A3L_fnc_openBuyVehicle";
		check = "(cursorObject getVariable 'vehiclePurchasable')";
	};
	class PickupItem {
		title = "Pickup Item";
		action = "[cursorObject] call A3L_fnc_pickupItem";
		check = "([cursorObject] call A3L_fnc_itemCheck)";
	};
	class WeaponCrafting {
		title = "Use Weapon bench";
		action = "call A3L_fnc_showGunCraftingDialog";
		check = "(cursorObject getVariable 'A3LWeaponBench')";
	};
	class PartsCrafting {
		title = "Use Parts bench";
		action = "call A3L_fnc_showPartsCraftingDialog";
		check = "(cursorObject getVariable 'A3LPartBench')";
	};
	class UseRefiner {
		title = "Use Refinery";
		action = "[cursorObject getVariable 'A3LSmelter']call A3L_fnc_showRefineryDialog";
		check = "(cursorObject getVariable 'A3LSmelter')";
	};
	class HarvestPlants {
		title = "Harvest Plant";
		action = "[cursorObject] spawn A3L_fnc_harvestPlant";
		check = "((typeOf cursorObject) IN (getArray(missionConfigFile >> 'Server_Settings' >> 'Player_Settings' >> 'Player_Farming' >> 'farmingPlants')))";
	};
	
	class PlantCannabisSeed {
		title = "Plant Cannabis Seed";
		action = "[0] call A3L_fnc_plantSeed";
		check = "((['Item_CannabisSeed'] call A3L_fnc_checkItem) >= 1)";
	};
	class PlantOpiumSeed {
		title = "Plant Opium Seed";
		action = "[1] call A3L_fnc_plantSeed";
		check = "((['Item_OpiumSeed'] call A3L_fnc_checkItem) >= 1)";
	};
	class PlantWheatSeed {
		title = "Plant Wheat Seed";
		action = "[2] call A3L_fnc_plantSeed";
		check = "((['Item_WheatSeed'] call A3L_fnc_checkItem) >= 1)";
	};
	class PlantCornSeed {
		title = "Plant Corn Seed";
		action = "[3] call A3L_fnc_plantSeed";
		check = "((['Item_CornSeed'] call A3L_fnc_checkItem) >= 1)";
	};
	class PlantBeanSeed {
		title = "Plant Bean Seed";
		action = "[4] call A3L_fnc_plantSeed";
		check = "((['Item_BeanSeed'] call A3L_fnc_checkItem) >= 1)";
	};
	class PlantCottonSeed {
		title = "Plant Cotton Seed";
		action = "[5] call A3L_fnc_plantSeed";
		check = "((['Item_CottonSeed'] call A3L_fnc_checkItem) >= 1)";
	};
	class PlantPumpkinSeed {
		title = "Plant Pumpkin Seed";
		action = "[6] call A3L_fnc_plantSeed";
		check = "((['Item_PumpkinSeed'] call A3L_fnc_checkItem) >= 1)";
	};
	class PlantSunflowerSeed {
		title = "Plant Sunflower Seed";
		action = "[7] call A3L_fnc_plantSeed";
		check = "((['Item_SunflowerSeed'] call A3L_fnc_checkItem) >= 1)";
	};
	
	class PoliceSubMenu {
		title = "Police Menu";
		action = "['cfgInteractionsPolice'] spawn A3L_fnc_InteractionSubRadial";
		check = "(A3L_copOnDuty)";
	};
	class BankMenu {
		title = "Access ATM";
		action = "[] spawn A3L_fnc_getBank";
		check = "(call A3L_fnc_atmCheck)";
	};
	class GiveDriver {
		title = "Get Driver License";
		action = "[player,1,0] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3LDMV')&&(player getVariable 'A3L_Driver_License' isEqualTo 0)";
	};
	class GiveTruck {
		title = "Get Truck License";
		action = "[player,1,1] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3LDMV')&&(player getVariable 'A3L_Truck_License' isEqualTo 0)";
	};
	class EMSMenu {
		title = "EMS Menu";
		action = "['cfgInteractionsEMS'] spawn A3L_fnc_InteractionSubRadial";
		check = "(A3L_medicOnDuty)";	
	};
	class RepairMenu {
		title = "Repair Menu";
		action = "['cfgInteractionRepair'] spawn A3L_fnc_InteractionSubRadial";
		check = "(isPlayer player)";
	};
	class RobStore {
		title = "Rob Store";
		action = "[cursorObject] spawn A3L_fnc_robbery";
		check = "(cursorObject getVariable 'RobberyType' isEqualTo 'Store') && (player distance cursorObject <= 5)";
	};
	class RobPharm {
		title = "Rob Pharmacy";
		action = "call A3L_fnc_robberyDialog";
		check = "(cursorObject getVariable 'RobberyType' isEqualTo 'Pharmacy') && (player distance cursorObject <= 5)";
	};
};

class cfgInteractionsSettings
{
	class AdminMenu {
		title = "Admin Menu";
		action = "[A3L_staffLevel] spawn A3L_fnc_openAdmin";
		check = "(A3L_staffLevel >=1)";
	};
	class SyncData {
		title = "Sync Data";
		action = "[] spawn A3L_fnc_updateRequest";
		check = "(isPlayer player)";
	};
	class showName {
		title = "Show my Name";
		action = "player setVariable ['A3L_NameSetting',true,true]";
		check = "(isPlayer player) && !(player getVariable 'A3L_NameSetting')";
	};
	class hideName {
		title = "Hide my Name";
		action = "player setVariable ['A3L_NameSetting',false,true]";
		check = "(isPlayer player) && (player getVariable 'A3L_NameSetting')";
	};
};

class cfgInteractionsPolice
{
	class CuffPerson {
		title = "Cuff Person";
		action = "[cursorObject] call A3L_fnc_cuffPerson";
		check = "(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) && (isPlayer cursorObject) && !(lifeState cursorObject isEqualTo 'INCAPACITATED') && !((getDammage cursorObject) isEqualTo 0.9)";
	};
	class RestrainingSubMenu {
		title = "Restrain Menu";
		action = "['cfgInteractionsPolice_Restraining'] spawn A3L_fnc_InteractionSubRadial";
		check = "!(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) && (isPlayer cursorObject)";
	};
	class SearchSubMenu {
		title = "Search Menu"; 
		action = "['cfgInteractionsPolice_Search'] spawn A3L_fnc_InteractionSubRadial";
		check = "!(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) && (isPlayer cursorObject)";
	};
	class Escort {
		title = "Escort Person";
		action = "[cursorObject,1] spawn A3L_fnc_escort;";
		check = "!(cursorObject getVariable 'A3L_drag') && (isPlayer cursorObject) && (cursorObject getVariable 'A3L_Cuffed' >= 1)";
	};
	class EscortStop {
		title = "Unescort Person";
		action = "[player,0] spawn A3L_fnc_escort; player forceWalk false; ";
		check = "(isPlayer player)";
	};
	class SeizeGroundItems {
		title = "Seize Items";
		action = "call A3L_fnc_seize";
		check = "(isPlayer player)";
	};
	class GiveTicket {
		title = "Give Ticket";
		action = "createDialog 'RscDisplay_Ticket';";
		check = "(isPlayer cursorObject)";
	};
	class VehicleSubMenu {
		title = "Vehicle Menu";
		action = "['cfgInteractionsPolice_Vehicles'] spawn A3L_fnc_InteractionSubRadial";
		check = "(isPlayer player)";
	};
	class LicenseSubMenu {
		title = "License Menu";
		action ="['cfgInteractionsPolice_License'] spawn A3L_fnc_InteractionSubRadial";
		check = "(isPlayer cursorObject)";
	};	
};
class cfgInteractionsPolice_Vehicles
{
	class MoveIn {
		title = "Move in Vehicle";
		action = "[(nearestObjects [player,['Car','Air','Boat'],5])] remoteExec ['A3L_fnc_movein',cursorObject getVariable 'CommunicationID']; ";
		check = "!(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) && (isPlayer cursorObject)";
	};
	class GrabKeys {
		title = "Grab Keys";
		action = "[cursorObject] call A3L_fnc_grabKeys";
		check = "!(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) && (isPlayer cursorObject)";
	};
	class GetPlate {
		title = "Get Plate";
		action = "hint format ['Vehicle Plate: %1',((cursorObject getVariable 'vehicleData') select 2) joinString ''];";
		check = "(cursorObject isKindOf 'Car')";
	};
};
class cfgInteractionsPolice_Restraining
{
	class UnCuffPerson {
		title = "unCuff Person";
		action = "cursorObject setVariable ['A3L_Cuffed',0,true]; [1] remoteExec ['A3L_fnc_restrainAdditions',cursorObject getVariable 'CommunicationID']; ['Item_Handcuff_Normal',1,true] call A3L_fnc_handleitem;";
		check = "(cursorObject getVariable 'A3L_Cuffed' isEqualTo 1) && (isPlayer cursorObject)";
	};
	class ForceOntoGround {
		title = "Force Ground";
		action = "[0] remoteExec ['A3L_fnc_restrainAdditions',cursorObject getVariable 'CommunicationID'];";
		check = "!(cursorObject getVariable 'A3L_Cuffed' isEqualTo 2) && (isPlayer cursorObject)";
	};
	class ForceOntoStand {
		title = "Force Stand";
		action = "[-1] remoteExec ['A3L_fnc_restrainAdditions',cursorObject getVariable 'CommunicationID'];";
		check = "(cursorObject getVariable 'A3L_Cuffed' isEqualTo 2) && (isPlayer cursorObject)";
	};
};

class cfgInteractionsPolice_Search
{
	class SearchPerson {
		title = "Strip Search Person";
		action = "[localize'STR_Notification_searchPlayer',10,'green'] call A3L_fnc_msg; [] remoteExec ['A3L_fnc_search',cursorObject getVariable 'CommunicationID'];";
		check = "!(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) && (isPlayer cursorObject)";
	};
	class PatdownPerson {
		title = "Patdown person";
		action = "[localize'STR_Notification_searchPlayer',10,'green'] call A3L_fnc_msg; [] remoteExec ['A3L_fnc_patDown',cursorObject getVariable 'CommunicationID'];";
		check = "!(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) && (isPlayer cursorObject)";		
	};
	class PatdownItems {
		title = "Search for items";
		action = "[localize'STR_Notification_searchPlayer',10,'green'] call A3L_fnc_msg; [player] remoteExec ['A3L_fnc_itemSearchPlayer',cursorObject getVariable 'CommunicationID'];";
		check = "!(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) && (isPlayer cursorObject)";		
	};
};

class cfgInteractionsPolice_License
{
	class Request {
		title = "Request License";
		action = "[cursorObject] call A3L_fnc_requestLicense";
		check = "(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) %%(isPlayer cursorObject)";
	};
	
	class Take {
		title = "Take License";
		action = "[cursorObject] call A3L_fnc_requestLicense";
		check = "!(cursorObject getVariable 'A3L_Cuffed' isEqualTo 0) && (isPlayer cursorObject)";
	};
	
	class GiveSubMenu {
		title = "Give License";
		action = "['cfgInteractionsPolice_LicenseGive'] spawn A3L_fnc_InteractionSubRadial";
		check = "(isPlayer cursorObject)";
	};
	class SuspendSubMenu {
		title = "Suspend License";
		action = "['cfgInteractionsPolice_LicenseSuspend'] spawn A3L_fnc_InteractionSubRadial";
		check = "(isPlayer cursorObject)";
	};
	class UnSuspendSubMenu {
		title = "UnSuspend License";
		action = "['cfgInteractionsPolice_LicenseUnSuspend'] spawn A3L_fnc_InteractionSubRadial";
		check = "(isPlayer cursorObject)";
	};
};

class cfgInteractionsPolice_LicenseGive
{
	class GivePilot {
		title = "Give Pilot License";
		action = "[cursorObject,1,2] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Pilot_License' isEqualTo 0)&&(isPlayer cursorObject)";
	};
	class GiveFirearm {
		title = "Give Firearm License";
		action = "[cursorObject,1,3] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Firearm_License' isEqualTo 0)&&(isPlayer cursorObject)";
	};
	class GiveRifle {
		title = "Give Rifle License";
		action = "[cursorObject,1,4] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Rifle_License' isEqualTo 0)&&(isPlayer cursorObject)";
	};
};

class cfgInteractionsPolice_LicenseSuspend
{
	class SuspendDriver {
		title = "Suspend Driver License";
		action = "[cursorObject,2,0] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Driver_License' isEqualTo 1)&&(isPlayer cursorObject)";
	};
	class SuspendTruck {
		title = "Suspend Truck License";
		action = "[cursorObject,2,1] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Truck_License' isEqualTo 1)&&(isPlayer cursorObject)";
	};
	class SuspendPilot {
		title = "Suspend Pilot License";
		action = "[cursorObject,2,2] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Pilot_License' isEqualTo 1)&&(isPlayer cursorObject)";
	};
	class SuspendFirearm {
		title = "Suspend Firearm License";
		action = "[cursorObject,2,3] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Firearm_License' isEqualTo 1)&&(isPlayer cursorObject)";
	};
	class SuspendRifle {
		title = "Suspend Rifle License";
		action = "[cursorObject,2,4] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Rifle_License' isEqualTo 1)&&(isPlayer cursorObject)";
	};
};

class cfgInteractionsPolice_LicenseUnSuspend
{
	class UnSuspendDriver {
		title = "UnSuspend Driver License";
		action = "[cursorObject,1,0] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Driver_License' isEqualTo 2)&&(isPlayer cursorObject)";
	};
	class UnSuspendTruck {
		title = "UnSuspend Truck License";
		action = "[cursorObject,1,1] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Truck_License' isEqualTo 2)&&(isPlayer cursorObject)";
	};
	class UnSuspendPilot {
		title = "UnSuspend Pilot License";
		action = "[cursorObject,1,2] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Pilot_License' isEqualTo 2)&&(isPlayer cursorObject)";
	};
	class UnSuspendFirearm {
		title = "UnSuspend Firearm License";
		action = "[cursorObject,1,3] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Firearm_License' isEqualTo 2)&&(isPlayer cursorObject)";
	};
	class UnSuspendRifle {
		title = "UnSuspend Rifle License";
		action = "[cursorObject,1,4] call A3L_fnc_licenseState";
		check = "(cursorObject getVariable 'A3L_Rifle_License' isEqualTo 2)&&(isPlayer cursorObject)";
	};
};

class cfgInteractionsEMS
{
	class ReviveOption {
		title = "Revive";
		action = "[cursorObject] spawn A3L_fnc_revive";
		check = "(isPlayer cursorObject) && (lifeState cursorObject	== 'INCAPACITATED')";	
	};
	
	class DOAOption {
		title = "Declare DOA";
		action = "[cursorObject] spawn A3L_fnc_doa";
		check = "(isPlayer cursorObject) && (lifeState cursorObject	== 'INCAPACITATED')";	
	};
	
	class DrugTest {
		title = "Urine Test";
		action = "[cursorObject] call A3L_fnc_drugTest";
		check = "(isPlayer cursorObject)";
	};
};

class cfgInteractionRepair
{
	class RepairFL{
		title = "Front Left Tyre";
		action = "[cursorObject,'FL'] spawn A3L_fnc_vehicleRepair";
		check = "[cursorObject,'FL'] call A3L_fnc_vehicleDamageCheck";		
	};
	class RepairBL{
		title = "Back Left Tyre";
		action = "[cursorObject,'BL'] spawn A3L_fnc_vehicleRepair";
		check = "[cursorObject,'BL'] call A3L_fnc_vehicleDamageCheck";		
	};
	class RepairFR{
		title = "Front Right Tyre";
		action = "[cursorObject,'FR'] spawn A3L_fnc_vehicleRepair";
		check = "[cursorObject,'FR'] call A3L_fnc_vehicleDamageCheck";		
	};
	class RepairBR{
		title = "Back Right Tyre";
		action = "[cursorObject,'BR'] spawn A3L_fnc_vehicleRepair";
		check = "[cursorObject,'BR'] call A3L_fnc_vehicleDamageCheck";		
	};
};