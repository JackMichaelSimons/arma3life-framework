class Server_Vehicles
{
	
	class ivory_mp4
	{
		buyValue = 80000;
		maxStore = 15;
		copVehicle = 0;
	};
	
	class ivory_f1
	{
		buyValue = 85000;
		maxStore = 15;
		copVehicle = 0;
	};
	
	class ivory_lfa
	{
		buyValue = 120000;
		maxStore = 15;
		copVehicle = 0;
	};
	
	class ivory_veyron
	{
		buyValue = 1000000;
		maxStore = 10;
		copVehicle = 0;
	};
	
	class ivory_evox
	{
		buyValue = 52000;
		maxStore = 25;
		copVehicle = 0;
	};
	class ivory_gt500
	{
		buyValue = 45000;
		maxStore = 15;
		copVehicle = 0;
	};
	class Jonzie_Raptor
	{
		buyValue = 38000;
		maxStore = 100;
		copVehicle = 0;
	};
	class A3L_Renault_Magnum_Black
	{
		buyValue = 97000;
		maxStore = 280;
		copVehicle = 0;
	};
	class ivory_suburban
	{
		buyValue = 73000;
		maxStore = 40;
		copVehicle = 0;
	};
	class Jonzie_Mini_Cooper
	{
		buyValue = 1200;
		maxStore = 15;
		copVehicle = 0;
	};
	class tw_explorer_marked
	{
		buyValue = 100;
		maxStore = 35;
		copVehicle = 1;
	};
	class ivory_evox_unmarked
	{
		buyValue = 100;
		maxStore = 25;
		copVehicle = 1;
	};
	class tw_durango_marked
	{
		buyValue = 100;
		maxStore = 45;
		copVehicle = 1;
	};
	class ivory_lp560_don2
	{
		buyValue = 120000;
		maxStore = 15;
		copVehicle = 0;
	};
	class tw_truck_blue
	{
		buyValue = 17000;
		maxStore = 65;
		copVehicle = 0;
	};
	class Urbanized_LaFerrari_black
	{
		buyValue = 160000;
		maxStore = 15;
		copVehicle = 0;
	};
	class ivory_e36
	{
		buyValue = 40000;
		maxStore = 25;
		copVehicle = 0;
	};
	class jonzie_ambulance_ems
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class ivory_b206_police
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class ivory_b206_rescue
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class ivory_elise
	{
		buyValue = 75000;
		maxStore = 15;
		copVehicle = 0;
	};
	class Urbanized_67Mustang_P
	{
		buyValue = 100;
		maxStore = 20;
		copVehicle = 1;	
	};
	
	class Urbanized_G65_DA
	{
		buyValue = 40000;
		maxStore = 30;
		copVehicle = 0;
	};
	class Urbanized_G65_DOJ
	{
		buyValue = 40000;
		maxStore = 30;
		copVehicle = 0;
	};
};
