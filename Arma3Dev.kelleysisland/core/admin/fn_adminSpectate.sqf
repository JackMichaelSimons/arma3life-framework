// Heisen
// Setup Spectator Mode - not completed


if (isNil "A3L_AdminSpector") then { 
	A3L_AdminSpector = false;
};

if (A3L_AdminSpector) then {
	A3L_AdminSpector = false;
	[localize"STR_AdminMenu_AdminSpectateOff",10,"blue"] call A3L_fnc_msg;
	[A3L_AdminSpectorGear] call A3l_fnc_setGear;
	[([player,"Spectate Deactivated","",""]),"ADMIN"] remoteExec ["BEC_fnc_log",2];
} else {
	A3L_AdminSpector = true;
	A3L_AdminSpectorGear = call A3L_fnc_getGear;
	call A3L_fnc_wipeGear;
	player forceAddUniform "U_B_Protagonist_VR"; //--- Admin Clothing
	[localize"STR_AdminMenu_AdminSpectateOn",10,"blue"] call A3L_fnc_msg;
	[([player,"Spectate Activated","",""]),"ADMIN"] remoteExec ["BEC_fnc_log",2];
	//call A3L_fnc_adminSpectateStart;
};
