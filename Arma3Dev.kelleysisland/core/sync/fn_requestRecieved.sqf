/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_requestRecieved.sqf
Author: Heisen http://heisen.pw
Description: Handle Inventory Menu
Parameter(s): N/A
************************************************************/


params [
	"_data"
];

A3L_recievedData = _data select 0;

A3L_copLevel = A3L_recievedData select 0;
player setVariable ["copLevel",A3L_copLevel,true];
A3L_medicLevel = A3L_recievedData select 1;
player setVariable ["medicLevel",A3L_medicLevel,true];
A3L_donLevel = A3L_recievedData select 2;
A3L_staffLevel = A3L_recievedData select 3;

_A3L_items = A3L_recievedData select 4;

{
	call compile format ["%1 = %2",_x select 0,_x select 1];
} forEach _A3L_items;


A3L_Hunger = A3L_recievedData select 5;
A3L_Thirst = A3L_recievedData select 6;

A3L_Bank = A3L_recievedData select 9;

[A3L_recievedData select 7] call A3L_fnc_setGear;

A3L_JailFetch = A3L_recievedData select 10;
A3L_MedicalBloodType = A3L_recievedData select 11;

if !(A3L_JailFetch isEqualTo "") then {
	[(A3L_JailFetch select 0 select 0),(A3L_JailFetch select 0 select 1),A3L_JailFetch select 1] call A3L_fnc_setupJail;
} else {
	
};


//--- Steve, you could do a count of licenses then for "_i"..
A3L_LicenseData = A3L_recievedData select 8;
//--- Do a check if data is "" blank
player setVariable["A3L_License",A3L_LicenseData,true];
player setVariable["A3L_Driver_License",A3L_LicenseData select 0 select 1,true];
player setVariable["A3L_Truck_License",A3L_LicenseData select 1 select 1,true];
player setVariable["A3L_Pilot_License",A3L_LicenseData select 2 select 1,true];
player setVariable["A3L_Firearm_License",A3L_LicenseData select 3 select 1,true];
player setVariable["A3L_Rifle_License",A3L_LicenseData select 4 select 1,true];




