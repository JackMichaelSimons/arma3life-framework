// Heisen
// Open Police Interaction

params [
	"_config"
];

closeDialog 0;

MB_SubMenuOverflowList = [];

_actions = "true" configClasses (missionConfigFile >> _config);


{
	_title = getText (_x >> "title");
	_func = getText (_x >> "action");
	_condition = getText (_x >> "check");
	
	if ((call compile _condition)) then {
		MB_SubMenuOverflowList pushback [_title,_func];
	};
} forEach _actions;

createDialog "MB_Interaction_Menu"; 
 
MB_actionList = []; 

_idd = 1001;

{ 
	ctrlSetText[_idd,(_x select 0)]; 
	_idd = _idd + 1; 
	MB_actionList pushBack _x; 
} forEach MB_SubMenuOverflowList;