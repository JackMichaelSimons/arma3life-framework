// Steve Adtur

disableSerialization;

params[
	"_refineryType"
];

private ["_countNum"];
_countNum = -1;

_itemArray = "true" configClasses (missionConfigFile >> "Server_Items");

_unrefinedArray = getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Unrefined");



createDialog "RscDisplay_RefiningWindow";

waitUntil {!isNull (findDisplay 19873);};


	{

			_varName = configName _x;
			_varAmount = call compile _varName;

			if(_varName IN _unrefinedArray) then {
			_refType = getText (_x >> "RefineryType");
				if(_refType == _refineryType)then{
					if(_varAmount >= 1)then{

					_countNum = _countNum + 1;
					_varDisplayName = getText (_x >> "displayName");
					lbAdd [1500,format["%1 x %2",_varAmount,localize(_varDisplayName)]];
					lbSetData[1500,_countNum,_varName];
					};
				};
			};

	} forEach _itemArray;

