// Steve Adtur

disableSerialization;

private ["_countNum"];
_countNum = -1;

_weaponsArray = "true" configClasses (missionConfigFile >> "Server_Crafting");


createDialog "RscDisplay_CraftingWindow";

waitUntil {!isNull (findDisplay 19872);};

{
	_varName = configName _x;

	_countNum = _countNum + 1;
	_varDisplayName = getText (_x >> "displayName");
	lbAdd [1500,format["%1",localize(_varDisplayName)]];
	lbSetData[1500,_countNum,_varName];

} forEach _weaponsArray;
