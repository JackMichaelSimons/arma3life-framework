/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_recieveTicket.sqf
Author: Heisen http://heisen.pw
Description: Recieve Ticket
Parameter(s): N/A
************************************************************/


params [
	"_reason",
	"_value",
	"_sender"
];

["You've recieved a ticket..",10,"red"] call A3L_fnc_msg;

createDialog "RscDisplay_RecieveTicket";

((findDisplay 19877)displayCtrl 1100) ctrlSetStructuredText parseText format ["Reason: %1",_reason];
((findDisplay 19877)displayCtrl 1101) ctrlSetStructuredText parseText format ["Value: %1",_value];

A3L_TicketRecentSender = _sender;
A3L_TicketRecentValue = _value;