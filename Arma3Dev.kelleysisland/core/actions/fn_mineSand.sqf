// Heisen


_surfaceType = surfaceType (getPos player);
_isSand = getArray (missionConfigFile >> "Server_Settings" >> "Player_Settings" >> "Player_Mining" >> "sandSurfaceTypes");


if !(_surfaceType IN _isSand) exitWith {
	[localize"STR_Notification_noSand",10,"red"] call A3L_fnc_msg;
};


A3L_SandMiner = A3L_SandMiner + round(random 5);
if (A3L_SandMiner >= 100) exitWith {
	A3L_SandMiner = 0;
	["Item_Sand",(round(random 3)),true] call A3L_fnc_handleItem;
	[localize"STR_Notification_sandMined",10,"green"] call A3L_fnc_msg;
};