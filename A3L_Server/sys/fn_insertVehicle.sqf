/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_insertVehicle.sqf
Author: Heisen http://heisen.pw
Description: Insert vehicle into vehicle table.
Parameter(s): N/A
************************************************************/


params [
	"_player",
	"_vehicle",
	"_colour"
];

//--- Generate License Plate
_license = call A3Lsys_fnc_generatePlate;
if (str(_license) isEqualTo "true") exitWith {[_player,_vehicle,_colour] call A3Lsys_fnc_insertVehicle;};

diag_log format ["A3L Server Debug Plate Result %1",_license];

_insertstr = format ["insertVehicle:%1:%2:%3:%4:%5:%6:%7:%8:%9:%10:%11",getPlayerUID _player,str(typeOf _vehicle),str(_license joinString ""),_colour,[],[],[],fuel _vehicle,1,0,"OUT"]; 
_insert = [0, _insertstr] call A3LMySQL_fnc_DBquery; 

diag_log format ["A3L Server Vehicle UID:(%1) VEHICLE:(%2) LICENSE:(%3) new data created.",getPlayerUID _player,typeOf _vehicle,_license];

//--- Set Vehicle lock status.
_vehicle setvehiclelock "LOCKED";

//--- Owner, Key Array.
_vehicle setVariable ["vehicleData",[getPlayerUID _player,[getPlayerUID _player],(_license joinString "")],true];

//--- License Plate Set
_vehicle setObjectTextureGlobal [4,format["\ivory_data\license\%1.paa",_license select 0]];
_vehicle setObjectTextureGlobal [5,format["\ivory_data\license\%1.paa",_license select 1]];
_vehicle setObjectTextureGlobal [6,format["\ivory_data\license\%1.paa",_license select 2]];
_vehicle setObjectTextureGlobal [7,format["\ivory_data\license\%1.paa",_license select 3]];

_vehicle setObjectTextureGlobal [8,format["\ivory_data\license\%1.paa",_license select 4]];
_vehicle setObjectTextureGlobal [9,format["\ivory_data\license\%1.paa",_license select 5]];
_vehicle setObjectTextureGlobal [10,format["\ivory_data\license\%1.paa",_license select 6]];

