/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_insertPlayers.sqf
Author: Heisen http://heisen.pw & Steve Adtur
Description: insert initial player into table
Parameter(s): N/A
************************************************************/


params [
	"_player"
];


//--- Setup Blood Type
_bloodTypes = getArray(missionConfigFile >> "Server_Medical" >> "bloodTypes");
_num = round(random [0,(((count _bloodTypes) - 1) / 2),((count _bloodTypes) - 1)]);

_myBloodType = [_num,_bloodTypes select _num select 0];

//--- Setup Licenses
_licenseArray = ([["STR_License_Driver",0],["STR_License_Truck",0],["STR_License_Pilot",0],["STR_License_Firearm",0],["STR_License_Rifle",0]]);
_player setVariable ["A3L_License",_licenseArray,true];
_player setVariable["A3L_Driver_License",_licenseArray select 0 select 1,true];
_player setVariable["A3L_Truck_License",_licenseArray select 1 select 1,true];
_player setVariable["A3L_Pilot_License",_licenseArray select 2 select 1,true];
_player setVariable["A3L_Firearm_License",_licenseArray select 3 select 1,true];
_player setVariable["A3L_Rifle_License",_licenseArray select 4 select 1,true];
//

_insertstr = format ["insertPlayer:%1:%2:%3:%4",getPlayerUID _player,name _player,_licenseArray,_myBloodType]; 
_insert = [0, _insertstr] call A3LMySQL_fnc_DBquery; 

diag_log format ["A3L Server Player %1 new data created.",getPlayerUID _player];

