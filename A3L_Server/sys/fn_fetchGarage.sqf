/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_fetchGarage.sqf
Author: Heisen http://heisen.pw
Description: Query DB for Vehicle listing under specific PUID.
Parameter(s): N/A
************************************************************/


params [
	"_player"
];

//--- Perform Query
_fetch = [format ["fetchGarage:%1", getPlayerUID _player], 2] call A3LMySQL_fnc_DBasync;

diag_log format ["A3L Server FETCH Owner:(%1) Garage:(%2)",getPlayerUID _player,_fetch];

//--- Send Query result to Client.
[_fetch] remoteExec ["A3L_fnc_remoteRecieved",_player getVariable "communicationID"];