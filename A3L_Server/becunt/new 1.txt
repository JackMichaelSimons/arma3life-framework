////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Heisen Beira-Mar, v1.063, #Tuqypi)
////////////////////////////////////////////////////////

class RscFrame_1800: RscFrame
{
	idc = 1800;
	x = 0.855781 * safezoneW + safezoneX;
	y = 0.247 * safezoneH + safezoneY;
	w = 0.128906 * safezoneW;
	h = 0.341 * safezoneH;
	colorBackground[] = {0,0,0,0.7};
};
class RscStructuredText_1100: RscStructuredText
{
	idc = 1100;
	text = "New Message: 0412215"; //--- ToDo: Localize;
	x = 0.860937 * safezoneW + safezoneX;
	y = 0.258 * safezoneH + safezoneY;
	w = 0.118594 * safezoneW;
	h = 0.022 * safezoneH;
	colorBackground[] = {0,0,0,0};
};
class RscStructuredText_1101: RscStructuredText
{
	idc = 1101;
	text = "This the message"; //--- ToDo: Localize;
	x = 0.860937 * safezoneW + safezoneX;
	y = 0.313 * safezoneH + safezoneY;
	w = 0.118594 * safezoneW;
	h = 0.264 * safezoneH;
	colorBackground[] = {0,0,0,0};
};
class RscPicture_1200: RscPicture
{
	idc = 1200;
	text = "P:\A3L_Textures\Dialogs\Phone\base.paa";
	x = 0.304062 * safezoneW + safezoneX;
	y = 0.17 * safezoneH + safezoneY;
	w = 0.391875 * safezoneW;
	h = 0.693 * safezoneH;
};
class RscPicture_1201: RscPicture
{
	idc = 1201;
	text = "P:\A3L_Textures\Dialogs\Phone\frame.paa";
	x = 0.304062 * safezoneW + safezoneX;
	y = 0.17 * safezoneH + safezoneY;
	w = 0.391875 * safezoneW;
	h = 0.693 * safezoneH;
};
class batpic: RscPicture
{
	idc = 1202;
	text = "P:\A3L_Textures\Dialogs\Phone\0bat.paa";
	x = 0.555688 * safezoneW + safezoneX;
	y = 0.258 * safezoneH + safezoneY;
	w = 0.0154688 * safezoneW;
	h = 0.011 * safezoneH;
};
class RscStructuredText_1102: RscStructuredText
{
	idc = 1102;
	text = "100%"; //--- ToDo: Localize;
	x = 0.422656 * safezoneW + safezoneX;
	y = 0.247 * safezoneH + safezoneY;
	w = 0.149531 * safezoneW;
	h = 0.022 * safezoneH;
	colorBackground[] = {0,0,0,0};
};
class RscListbox_1500: RscListbox
{
	idc = 1500;
	x = 0.438125 * safezoneW + safezoneX;
	y = 0.335 * safezoneH + safezoneY;
	w = 0.12375 * safezoneW;
	h = 0.099 * safezoneH;
};
class RscButton_1600: RscButton
{
	idc = 1600;
	x = 0.437094 * safezoneW + safezoneX;
	y = 0.4406 * safezoneH + safezoneY;
	w = 0.12375 * safezoneW;
	h = 0.088 * safezoneH;
};
class RscButton_1601: RscButton
{
	idc = 1601;
	text = "Send"; //--- ToDo: Localize;
	x = 0.533001 * safezoneW + safezoneX;
	y = 0.676 * safezoneH + safezoneY;
	w = 0.0309375 * safezoneW;
	h = 0.022 * safezoneH;
};
class RscEdit_1400: RscEdit
{
	idc = 1400;
	x = 0.438125 * safezoneW + safezoneX;
	y = 0.5748 * safezoneH + safezoneY;
	w = 0.12375 * safezoneW;
	h = 0.099 * safezoneH;
};
class RscEdit_1401: RscEdit
{
	idc = 1401;
	x = 0.435032 * safezoneW + safezoneX;
	y = 0.676 * safezoneH + safezoneY;
	w = 0.0979687 * safezoneW;
	h = 0.022 * safezoneH;
};
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT END
////////////////////////////////////////////////////////
