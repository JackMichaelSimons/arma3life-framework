// Heisen


diag_log "BECUNT :: Running becunt";


/*------* configuration *------*/
	_serverCommandPassword = "1Pm02ncs8AWg";
/*------*/


//--- Load Data Values
call bec_fnc_data;


//--- Base Send Array
_toSend = []; 


//--- Check for BAD CfgPatches in Addons not Authorised!
_checkCfgPatches = {
	{
		if !((configName _x) IN ((call ArmA3_whitelistedCfgPatches)+(call Modded_whitelistedCfgPatches))) exitWith {
			[([player,"HACKER - BAD CfgPatches",configName _x,""]),"HACKER"] remoteExec ["BEC_fnc_log",2];
		};
	} forEach ("true" configClasses(configFile >> "cfgPatches"));
};
_toSend pushback _checkCfgPatches;


//--- Checking for BAD Variables
_checkBadVariables = {
	{
		_var = str _x;
		if (!isNil _x) exitWith {
			[([player,format["Forbidden Value set to %1",call compile _var],_x,call compile _var]),"HACKER"] remoteExec ["BEC_fnc_log",2];
		};
	} forEach (call Modded_logVariables);
};
_toSend pushback _checkBadVariables;


//--- Checking for BAD action menu strings
_checkBadAddAction = {
	_actionIDS = actionIDs player;
	{
		_action = player actionParams _x;
		_log = format ["%1 - %2",_action select 0,_action select 1];
		[([player,_log,str(_x),str(_action)]),"HACKER"] remoteExec ["BEC_fnc_log",2];
	} forEach _actionIDS;
};
_toSend pushback _checkBadAddAction;






//--- Send Request to Client randomly & check client. go through array call, deleteAt the array after called, send data if need to nil whole array.	