A3L_RespawnVehicle = [];

[] spawn {
	for "_i" from 0 to 1 step 0 do {
		[getMarkerPos "A3L_Mine_1"] call A3Lsys_fnc_spawnRocks;
		[getMarkerPos "A3L_Tree_1"] call A3Lsys_fnc_spawnTrees;
		call A3Lsys_fnc_respawnNewVehicleLoop;
		sleep 30;
	};
};

[] spawn A3Lsys_fnc_cleanup;
[] spawn A3Lsys_fnc_handlePay;

//--- Base Tax
A3L_Tax = 1.00;
publicVariable "A3L_Tax";

A3L_Server_isReady = true;
publicVariable "A3L_Server_isReady";

diag_log "A3L Server Ready!";

