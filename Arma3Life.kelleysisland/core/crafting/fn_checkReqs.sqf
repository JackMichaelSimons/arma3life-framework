// Steve Adtur
_ctrl1 = (findDisplay 19872) displayCtrl 1100;
_itemNumber = lbCurSel 1500;
_itemName = lbData [1500,_itemNumber];

_itemArray = getArray(missionConfigFile >> "Server_Items" >> "Server_Items_Crafting");


_outputString = "Requirements: <br />";

if(_itemName IN _itemArray) then {
	_materialsReq = getArray(missionConfigFile >> "Server_Items" >> _itemName >> "materialsNeeded");
	{
		_outputString = format["%1 %2 x %3 <br />", _outputString, _x select 1,localize(_x select 2)];
		
		
	}foreach _materialsReq;
	_ctrl1 ctrlSetStructuredText parseText _outputString;

}else{
	_itemArray = getArray(missionConfigFile >> "Server_Crafting" >> "Server_Crafting_Guns");
	if(_itemName IN _itemArray) then {
		_materialsReq = getArray(missionConfigFile >> "Server_Crafting" >> _itemName >> "partsNeeded");
		_rifleReq = getText(missionConfigFile >> "Server_Crafting" >> _itemName >> "rifleNeededName");
		if(rifleReq != "")then{
			_outputString = format["%1 %2 x %3 <br />", _outputString, "1", _rifleReq];
		};
		{
		_outputString = format["%1 %2 x %3 <br />", _outputString, _x select 1, localize(_x select 2)];
		
		
		}foreach _materialsReq;
	_ctrl1 ctrlSetStructuredText parseText _outputString;

	};
};