/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_updateRequest.sqf
Author: Heisen http://heisen.pw
Description: Save Items
Parameter(s): N/A
************************************************************/


if (A3L_SyncRecent) exitWith {[localize "STR_Notification_SyncRecent",10,"blue"] call A3L_fnc_msg;};
A3L_SyncRecent = true;

private ["_itemsSave"];
_itemsSave = [];

_items = "true" configClasses (missionConfigFile >> "Server_Items");

{
	if ((call compile(configName _x)) >= 1) then {
		_itemsSave pushback [configName _x,call compile(configName _x)];
	};
} forEach _items;
[localize "STR_Notification_SyncData",10,"blue"] call A3L_fnc_msg;

//--- Object, Data to Save (Items.)
[player,[_itemsSave,A3L_Hunger,A3L_Thirst,(call A3L_fnc_getGear)]] remoteExec ["A3Lsys_fnc_updatePlayer",2];

sleep 300;
A3L_SyncRecent = false;

