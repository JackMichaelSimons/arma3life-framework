/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_pickupItem.sqf
Author: Heisen http://heisen.pw
Description: Pickup Item? Self Explained..
Parameter(s): N/A
************************************************************/


params [
	"_item"
];

systemChat format ["%1",_item];

[((_item getVariable "Server_Item_Data") select 0),((_item getVariable "Server_Item_Data") select 1),true] call A3L_fnc_handleItem;
if((['Item_SpareWheel'] call A3L_fnc_checkItem) >= 1)then{player forceWalk true;}else{player forceWalk false};
player playMove "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";
deleteVehicle _item;	
