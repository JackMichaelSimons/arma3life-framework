// Steve Adtur, Heisen

disableSerialization;


private ["_isDone","_display","_pos","_exit"];
_isDone = false;
_exit = false;

if (A3L_antiSpam) exitWith {
	["You're already refining..",10,"red"] call A3L_fnc_msg;
};
A3L_antiSpam = true;

		["Repairing",10,"blue"] call A3L_fnc_msg;
		
		995 cutRsc ["RscTitle_ProgressBar","PLAIN"];	// Show Progress Bar
			 
		_display = (uiNameSpace getVariable "RscTitle_ProgressBar");
		_text = "Repairing";
		_pos = getPos player;
		
		for "_i" from 0 to 60 step +1 do {
			_current = progressPosition ((_display)displayCtrl 1001);
			((_display)displayCtrl 1001) progressSetPosition (_current + (1 / 60));
			((_display)displayCtrl 1100) ctrlSetStructuredText parseText format ["%1 | %2%3",_text,round(_current * 100),"%"];
			sleep 1;
			if (player distance _pos > 10) exitWith {
				995 cutText ["","PLAIN"];
				A3L_antiSpam = false;
				_exit = true;
			};
		};
		if (_exit) exitWith {
			["You moved too far away..",10,"red"] call A3L_fnc_msg;
		};
		
		_isDone = true;
		
		waitUntil {_isDone}; // Wait till progress is done
		
		
		["Repairing Completed",10,"green"] call A3L_fnc_msg;
		995 cutText ["","PLAIN"]; // Remove Progress Bar
		A3L_antiSpam = false;
		cursorObject setDamage 0;
