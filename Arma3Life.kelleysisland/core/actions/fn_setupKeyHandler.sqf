/************************************************************
(C) Heisen, contactheisen@gmail.com
Created for ArmA3Life http://www.arma-life.com/forums
File: fn_setupKeyHandler.sqf
Author: Heisen http://heisen.pw
Description: Setup Key Handler
Parameter(s): N/A
************************************************************/


params ["_keyData"];

_key = _keyData select 1;
_shift = _keyData select 2;
_ctrl = _keyData select 3;
_alt = _keyData select 4;

_interactionKey = if (count (actionKeys "User10") isEqualTo 0) then {219} else {(actionKeys "User10") select 0};

handled = false;

switch (_key) do {

	case 1: {
		if !(isNil "A3L_ShopShowClass") then {
			removeUniform player;
			removeVest player;
			removeBackpack player;
			removeHeadgear player;
			
			player forceAddUniform (A3L_ShopShowClass select 0);
			player addVest (A3L_ShopShowClass select 1);
			player addBackpack (A3L_ShopShowClass select 2);
			player addHeadgear (A3L_ShopShowClass select 3);
			{
				player addItem _x;
				player assignItem _x;
			} forEach (A3L_ShopShowClass select 4);
			{
				player addItemToBackpack _x;
			} forEach (A3L_ShopShowClass select 5);
			player addWeapon (A3L_ShopShowClass select 6);
			{
				player addMagazine _x;
			} forEach (A3L_ShopShowClass select 7);
			A3L_ShopShowClass = nil;
			shopCamera cameraEffect ["terminate","back"];
			camDestroy shopCamera;
			systemChat "Reset Gear after purchasess if any.";
		};
		if(A3L_Spawning)then{
			call A3L_fnc_spawnMenu;
		};
		if(A3L_RespawnReady)then{
			createDialog "RscDisplay_Respawn";
		};
		A3L_SyncRecent = false;
		[] spawn A3L_fnc_updateRequest;
		
		handled = false;
	};
	
	/*--- Shift + E
	case 18: {
		if (_shift) then {
			if (A3L_inAnimation) exitWith {};
			A3L_inAnimation = true;
			[true,"PUNCH"] spawn A3L_fnc_doAction;
		};
		handled = true;
	};
	*/
	//--- Windows Key? Thanks TONIC!!!
	case _interactionKey: {	
		if(!dialog)then{
			[cursorObject] call A3L_fnc_loadInteractionRadial;
			handled = true;
		};
	};
	
	//--- Unlock/Lock Vehicle
	case 22: {
	
		if ((cursorObject isKindOf "Car") OR (cursorObject isKindOf "Air")) then {
			_vehicleData = cursorObject getVariable "vehicleData";
			if ((getPlayerUID player) IN (_vehicleData select 1)) then {
				if ((locked cursorObject) == 2) then {
					cursorObject setVehicleLock "UNLOCKED";
					hint "Vehicle Unlocked.";
				} else {
					
					cursorObject setVehicleLock "LOCKED";
					hint "Vehicle Locked.";
				};
			} else {
				hint "You don't have Keys for this Vehicle!";
			};
		};
		_veh = vehicle player;
		if(_veh != player)then{
			if ((_veh isKindOf "Car") OR (_veh isKindOf "Air")) then {
				_vehicleData = _veh getVariable "vehicleData";
				if ((getPlayerUID player) IN (_vehicleData select 1)) then {
					if ((locked _veh) == 2) then {					
						_veh setVehicleLock "UNLOCKED";
						hint "Vehicle Unlocked.";
					} else {
						_veh setVehicleLock "LOCKED";
						hint "Vehicle Locked.";
					};
				} else {
					hint "You don't have Keys for this Vehicle!";
				};
			};
		};
		handled = true;
	};
	
	case 35: {
		if(_shift)then{
			if(currentWeapon player != "")then{
				player action ["SwitchWeapon", player, player, 100];
			};
		
		};
	
	handled = true;
	};

};

handled;