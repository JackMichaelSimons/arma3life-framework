// Heisen


params [
	"_commit",
	"_type"
];

if ((player getVariable "A3L_Cuffed") >=1) exitWith {A3L_inAnimation = false;};
if !((currentWeapon player) isEqualTo "") exitWith {A3L_inAnimation = false;};

if (_commit) then {
	if (_type isEqualTo "PUNCH") then {
		[player,"A3L_Anim_Ply_Punch_Right_Direct"] call A3L_fnc_doAnim;
		
		if !(isPlayer cursorObject) exitWith {[localize"STR_Notification_Punch_NoPlayerFound",10,"red"] call A3L_fnc_msg;A3L_inAnimation = false;};
		if (player distance cursorObject > 1.5) exitWith {[localize"STR_Notification_Punch_NoPlayerFound",10,"red"] call A3L_fnc_msg;A3L_inAnimation = false;};
		if (lifeState cursorObject isEqualTo 'INCAPACITATED') exitWith {[localize"STR_Notification_Punch_NoPlayerFound",10,"red"] call A3L_fnc_msg;A3L_inAnimation = false;};
		
		[localize"STR_Notification_Punch_HitPerson",10,"green"] call A3L_fnc_msg;
		[player,"punch"] call A3L_fnc_say3D;
		[false,"PUNCH"] remoteExec ["A3L_fnc_doAction",(cursorObject getVariable "CommunicationID")];
	};
} else {
	if (_type isEqualTo "PUNCH") then {
		_currentDamage = getDammage player;
		_add = 0.0666666666666667;
		if (((_currentDamage) + _add) >=0.9) exitWith {
			player setVariable ['A3L_Cuffed',0,true]; 
			[1] call A3L_fnc_restrainAdditions;
			player setDamage 0.9;
			player setUnconscious true;
			player allowDamage false;
			systemChat format ["Life state of player = %1" ,lifeState player];
			player setHitIndex [_hitIndex,0.9];
			_hitOut = format["hitIndex%1",_hitIndex];
			player setVariable[_hitOut,0.9,true];
			[] spawn A3L_fnc_respawn;
		};
		player setDamage (_currentDamage + _add);
		[localize"STR_Notification_FeelingPain",10,"green"] call A3L_fnc_msg;
		[player,"grunt"] call A3L_fnc_say3D;
	};
};

sleep 5;
A3L_inAnimation = false;

// A3L_Sound_Grunt
// A3L_Sound_Punch [player,"A3L_Sound_Grunt",5]A3L_inAnimation = false;