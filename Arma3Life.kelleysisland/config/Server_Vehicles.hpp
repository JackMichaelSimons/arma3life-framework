class Server_Vehicles
{
	
	class ivory_mp4
	{
		buyValue = 80000;
		maxStore = 15;
		copVehicle = 0;
	};
	
	class ivory_f1
	{
		buyValue = 85000;
		maxStore = 15;
		copVehicle = 0;
	};
	
	class ivory_lfa
	{
		buyValue = 120000;
		maxStore = 15;
		copVehicle = 0;
	};
	
	class ivory_veyron
	{
		buyValue = 1000000;
		maxStore = 10;
		copVehicle = 0;
	};
	
	class ivory_evox
	{
		buyValue = 52000;
		maxStore = 25;
		copVehicle = 0;
	};
	class ivory_gt500
	{
		buyValue = 45000;
		maxStore = 15;
		copVehicle = 0;
	};
	class Jonzie_Raptor
	{
		buyValue = 38000;
		maxStore = 100;
		copVehicle = 0;
	};
	class A3L_Renault_Magnum_Black
	{
		buyValue = 97000;
		maxStore = 280;
		copVehicle = 0;
	};
	class ivory_suburban
	{
		buyValue = 73000;
		maxStore = 40;
		copVehicle = 0;
	};
	class Jonzie_Mini_Cooper
	{
		buyValue = 1200;
		maxStore = 15;
		copVehicle = 0;
	};
	class tw_explorer_marked
	{
		buyValue = 100;
		maxStore = 35;
		copVehicle = 1;
	};
	class ivory_evox_unmarked
	{
		buyValue = 100;
		maxStore = 25;
		copVehicle = 1;
	};
	class tw_durango_marked
	{
		buyValue = 100;
		maxStore = 45;
		copVehicle = 1;
	};
	class red_charger_12_black
	{
		buyValue = 35000;
		maxStore = 25;
		copVehicle = 0;
	};
	class red_charger_15_black
	{
		buyValue = 53000;
		maxStore = 25;
		copVehicle = 0;
	};
	class red_panamera_10_darkred
	{
		buyValue = 82000;
		maxStore = 15;
		copVehicle = 0;
	};
	class red_venomgt_11_green
	{
		buyValue = 140000;
		maxStore = 15;
		copVehicle = 0;
	};
	class red_camaro_12_white
	{
		buyValue = 30000;
		maxStore = 15;
		copVehicle = 0;
	};
	class red_impala_64_black
	{
		buyValue = 12500;
		maxStore = 15;
		copVehicle = 0;
	};
	class red_savana_04_red
	{
		buyValue = 42500;
		maxStore = 115;
		copVehicle = 0;
	};
	class ivory_lp560_don2
	{
		buyValue = 120000;
		maxStore = 15;
		copVehicle = 0;
	};
	class tw_truck_blue
	{
		buyValue = 17000;
		maxStore = 65;
		copVehicle = 0;
	};
	class Urbanized_LaFerrari_black
	{
		buyValue = 160000;
		maxStore = 15;
		copVehicle = 0;
	};
	class ivory_e36
	{
		buyValue = 40000;
		maxStore = 25;
		copVehicle = 0;
	};
	class red_cvpi_06_aqua
	{
		buyValue = 12000;
		maxStore = 15;
		copVehicle = 0;
	};
	class red_f350_08_black
	{
		buyValue = 34000;
		maxStore = 75;
		copVehicle = 0;
	};
	class red_x6_10_green
	{
		buyValue = 80000;
		maxStore = 15;
		copVehicle = 0;		
	};
	class red_fusion_10_aqua
	{
		buyValue = 25000;
		maxStore = 25;
		copVehicle = 0;
	};
	class red_challenger_15_black
	{
		buyValue = 33000;
		maxStore = 25;
		copVehicle = 0;
	};
	class red_panamera_10_yellow
	{
		buyValue = 82000;
		maxStore = 15;
		copVehicle = 0;
	};
	class red_camaro_12_green
	{
		buyValue = 30000;
		maxStore = 25;
		copVehicle = 0;
	};
	class red_f12_13_black
	{
		buyValue = 100000;
		maxStore = 25;
		copVehicle = 0;
	};
	class red_porsche_12_green
	{
		buyValue = 82000;
		maxStore = 15;
		copVehicle = 0;
	};
	class red_vanquish_13ink
	{
		buyValue = 100000;
		maxStore = 15;
		copVehicle = 0;
	};
	class red_tahoe_13_black
	{
		buyValue = 23000;
		maxStore = 35;
		copVehicle = 0;
	};
	class red_firetruck_p_base
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class red_pumper_10_p_red
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class jonzie_ambulance_ems
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class red_charger_12_p_sheriff
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class red_charger_12_p_u_white
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class red_cvpi_06_p_traffic
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class red_cvpi_06_p_sheriff
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class red_cvpi_06_p_custom1
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class ivory_b206_police
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class ivory_b206_rescue
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class ivory_elise
	{
		buyValue = 75000;
		maxStore = 15;
		copVehicle = 0;
	};
	class red_taurus_10_p_blue
	{
		buyValue = 28000;
		maxStore = 30;
		copVehicle = 0;
	};
	class red_towtruck_08_black
	{
		buyValue = 8000;
		maxStore = 80;
		copVehicle = 0;
	};
	class red_suburban_15_e_EMS
	{
		buyValue = 100;
		maxStore = 50;
		copVehicle = 1;
	};
	class Urbanized_67Mustang_P
	{
		buyValue = 100;
		maxStore = 20;
		copVehicle = 1;	
	};
	class red_taurus_10_p_sheriff
	{
		buyValue = 100;
		maxStore = 30;
		copVehicle = 1;
	};
	class red_tahoe_13_p_u_sheriff
	{
		buyValue = 100;
		maxStore = 30;
		copVehicle = 1;
	};
	class red_suburban_15_p_sheriff
	{
		buyValue = 100;
		maxStore = 30;
		copVehicle = 1;
	};
	class red_challenger_15_p_u_white
	{
		buyValue = 500;
		maxStore = 20;
		copVehicle = 1;
	};
	class red_charger_15_p_sheriff
	{
		buyValue = 100;
		maxStore = 10;
		copVehicle = 1;		
	};
	class red_explorer_16_p_sheriff
	{
		buyValue = 100;
		maxStore = 30;
		copVehicle = 1;
	};
	class Urbanized_G65_DA
	{
		buyValue = 40000;
		maxStore = 30;
		copVehicle = 0;
	};
	class Urbanized_G65_DOJ
	{
		buyValue = 40000;
		maxStore = 30;
		copVehicle = 0;
	};
	class ivory_supra
	{
		buyValue = 20000;
		maxStore = 30;
		copVehicle = 0;
	};
};
