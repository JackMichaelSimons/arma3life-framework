class Server_Medical
{
	
	
	//--- Blood Type, Give to Type, Recieve from Type
	bloodTypes[] = {
		{"A+",{"A+","AB+"},{"A+","A-","O+","O-"}},
		{"O+",{"O+","A+","B+","AB+"},{"O+","O-"}},
		{"B+",{"B+","AB+"},{"B+","B-","O+","O-"}},
		{"AB+",{"AB+"},{"A+","O+","B+","AB+","A-","O-","B-","AB-"}},
		{"A-",{"A+","A-","AB+","AB-"},{"A-","O-"}},
		{"O-",{"A+","O+","B+","AB+","A-","O-","B-","AB-"},{"O-"}},
		{"B-",{"B+","B-","AB+","AB-"},{"B-","O-"}},
		{"AB-",{"AB+","AB-"},{"AB-","A-","B-","O-"}}
	};
	
	
};