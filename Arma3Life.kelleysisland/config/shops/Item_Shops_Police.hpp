Items_Shops_Police[] = {
	
	// Uniforms
	"Item_A3L_Patrol_Cadet_Uni",
	"Item_A3L_Patrol_Deputy_Uni",
	"Item_A3L_Patrol_Corporal_Uni",
	"Item_A3L_Patrol_Sergeant_Uni",
	"Item_A3L_Patrol_Lieutenant",
	"Item_A3L_Patrol_Captain_Uni",
	"Item_A3L_Patrol_UnderSheriff_Uni",
	"Item_A3L_Patrol_Sheriff_Uni",
	
	// Vests
	"Item_TRYK_V_Bulletproof_ECSO",
	
	// Backpacks
	"Item_TRYK_B_Belt_BLK",
	
	// Hats
	"Item_pmc_earpiece",
	"Item_TRYK_H_woolhat_tan",
	
	// Weapons
	"Item_RH_M4SBR",
	"Item_RH_30Rnd_556x45_M855A1"
};
